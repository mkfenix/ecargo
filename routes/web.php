<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('principal');
});
Route::resource('pais','PaisController');
Route::resource('empresa','EmpresaController');
Route::resource('sucursal','SucursalController');
Route::resource('condicion','CondicionController');
Route::resource('moneda','MonedaController');
Route::resource('contenedor','ContenedorController');  
Route::resource('destino','DestinoController');
Route::resource('emision','EmisionController');
Route::resource('ciudad','CiudadController');
Route::resource('cliente','ClienteController');
Route::resource('contacto','ContactoController');
Route::resource('proveedor','ProveedorController');
Route::resource('identificacion','IdentificacionController');

//PARA ENVIAR LA ID DE CLIENTE AL CONTROLADOR ContactoController
//FORMA 1 FUNCIONA
Route::get('cliente/{cliente}/contacto','ContactoController@index');

//PARA ENVIAR LA ID DE CLIENTE AL CONTROLADOR ContactoController AdicionarClienteContacto
Route::get('contacto/{cliente}/contacto','ContactoController@AdicionarClienteContacto');

//PARA ENVIAR LA ID DE CLIENTE AL CONTROLADOR ContactoController
Route::get('proveedor/{proveedor}/contacto','ContactoController@index');
