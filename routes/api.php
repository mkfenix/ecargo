<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Para Ajax de Cliente el que sirve para generar Select Ciudades
Route::post('url-ajaxCuidad','ClienteController@retornaCiudadAjax');
Route::post('url-ajaxContacto','ContactoController@retornaContactoAjax');