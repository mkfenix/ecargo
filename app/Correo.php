<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Correo extends Model
{
	protected $table = 'correo';
    protected $fillable = ['correo_descripcion','correo_estado','em_id','cli_id','cont_id','nav_id'];
    public $timestamps = false;
    protected $primaryKey = 'correo_id';
	
	public function cliente()
	{
	    return $this->belongsTo(Cliente::class,'cli_id');
	}
}
