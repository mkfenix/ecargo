<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contenedor extends Model
{
    //
    protected $table='contenedor';
    protected $fillable=['cont_tamano','cont_tipo','cont_descripcion'];
    public $timestamps=false;
    protected $primaryKey='cont_id';
}
