<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $table = 'contacto';
    protected $fillable = ['cont_nombre','cont_estado','cli_id','prov_id','nav_id'];
    public $timestamps = false;
    protected $primaryKey = 'cont_id';

    public function telefonos()
	{
	    return $this->hasMany(Telefono::class,'cont_id');
	}

	public function direcciones()
	{
	    return $this->hasMany(Direccion::class,'cont_id');
	}

	public function correos()
	{
	    return $this->hasMany(Correo::class,'cont_id');
	}
	
	public function cliente()
	{
	    return $this->belongsTo(Cliente::class,'cli_id');
	}
	public function proveedor()
	{
	    return $this->belongsTo(Proveedor::class,'prov_id');
	}
}
