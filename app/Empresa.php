<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    //
    protected $table = 'empresa';
    protected $fillable = ['emp_nombre','emp_codigo','emp_tipo_doc','emp_numDoc','emp_dircentral','emp_telefonos','emp_logo','emp_estado'];
    public $timestamps = false;
    protected $primaryKey = 'emp_id';

    //INTENTAR USAR
    public function direccion()
    {
    	return $this->hasMany('App\Direccion');
    }    
}
