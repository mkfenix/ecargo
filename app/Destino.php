<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destino extends Model
{
    //
    protected $table = 'destino';
    protected $fillable=['dest_descripcion'];
    public $timestamps=false;
    protected $primaryKey='dest_id';
}
