<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moneda extends Model
{
    //
    protected $table = 'moneda';
    protected $fillable=['mon_codigo, mon_descripcion'];
    public $timestamps = false;
    protected $primaryKey = 'mon_id';
}
