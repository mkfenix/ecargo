<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    //
    protected $table='sucursal';
    protected $fillable=['suc_ciudad','suc_estado'];
    public $timestamps=false;
    protected $primaryKey='suc_id';
}