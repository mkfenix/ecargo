<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condicion extends Model
{
    //
    protected $table = 'condicion';
    protected $fillable=['cond_codigo, cond_descripcion'];
    public $timestamps = false;
    protected $primaryKey = 'cond_id';
}
