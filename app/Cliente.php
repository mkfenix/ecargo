<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $fillable = ['cli_nombreEmpresa','cli_nombresPersona','cli_apellidosPersona','cli_tipo_doc','cli_num_doc','cli_ci'];
    public $timestamps = false;
    protected $primaryKey = 'cli_id';
	
	public function telefonos()
	{
	    return $this->hasMany(Telefono::class,'cli_id');
	}

	public function direcciones()
	{
	    return $this->hasMany(Direccion::class,'cli_id');
	}

	public function correos()
	{
	    return $this->hasMany(Correo::class,'cli_id');
	}

	public function contactos()
	{
	    return $this->hasMany(Contacto::class,'cli_id');
	}
}
