<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emision extends Model
{
    //
    protected $table ='emision';
    protected $fillable=['emi_descripcion'];
    public $timestamps=false;
    protected $primaryKey='emi_id';
}
