<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarFormularioProveedor extends FormRequest
{    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'prov_numDoc'       => 'required|min:5',
            'nombre'            => 'required|min:5',
            'direcciones'       => 'required|array',
            'direcciones.*'     => 'required|string|distinct|min:5',
            'telefonos'         => 'required|array',
            'telefonos.*'       => 'required|regex:/^[0-9+() -]+$/|distinct',
            'correos'           => 'required|array',
            'correos.*'         => 'required|email:rfc,dns|distinct',
        ];
    }

    public function messages()
    {
        return [
            'prov_numDoc.required'    => 'El :attribute es obligatorio.',
            'prov_numDoc.min'         => 'El :attribute debe ser mayor a 5 caracteres.',
            'nombre.required'   => 'El :attribute es obligatorio.',
            'nombre.min'        => 'El :attribute debe ser mayor a 5 caracteres.',
            'direcciones.*.required'    => 'La :attribute es obligatoria.',
            'direcciones.*.distinct'    => 'La :attribute debe ser única.',
            'direcciones.*.min'         => 'La :attribute debe ser mayor a 5 caracteres.',
            'telefonos.*.required'    => 'El :attribute es obligatorio.',
            'telefonos.*.regex'       => 'El formato del :attribute no es válido',
            'correos.*.required'    => 'El :attribute es obligatorio.',
            'correos.*.email'       => 'El :attribute no es un correo váldo..',
            'correos.*.distinct'    => 'El :attribute debe ser único.',
        ];
    }

    public function attributes()
    {
          return [
            'prov_numDoc'           => 'número de documento',
            'nombre'                => 'nombre de proveedor',
            'direcciones.0'         => 'dirección #1 de proveedor',
            'direcciones.1'         => 'dirección #2 de proveedor',
            'direcciones.2'         => 'dirección #3 de proveedor',
            'direcciones.3'         => 'dirección #4 de proveedor',
            'telefonos.0'       => 'teléfono #1 de proveedor',
            'telefonos.1'       => 'teléfono #2 de proveedor',
            'telefonos.2'       => 'teléfono #3 de proveedor',
            'telefonos.3'       => 'teléfono #4 de proveedor',
            'correos.0'         => 'correo #1 de proveedor',
            'correos.1'         => 'correo #2 de proveedor',
            'correos.2'         => 'correo #3 de proveedor',
            'correos.3'         => 'correo #4 de proveedor',
          ];
    }
}
