<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        if($request){
            $sql=trim($request -> get('buscarTexto'));
            $empresa = Empresa::where ('emp_nombre','LIKE','%'.$sql.'%')            
            ->orderBy ('emp_nombre','asc')
            ->paginate (5);
             $empresa->each(function($item){
                $item->emp_telefonos = explode(',', $item->emp_telefonos);
            });
            
            return view('empresa.index',["empresa"=>$empresa,"buscarTexto"=>$sql]);                
        }
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $empresa = new Empresa();
        $empresa->emp_nombre = $request->nombre;
        $empresa->emp_codigo = $request->codigo;
        //SELECT TIPO DOCUMENTO SE MANEJA DE LA MISMA FORMA QUE LOS DEMAS CAMPOS
        $empresa->emp_tipo_doc = $request->tipo_doc;        
        $empresa->emp_num_doc = $request->num_doc;
        $empresa->emp_dircentral = $request->dircentral;        
        $empresa->emp_telefonos = implode(',',$request->telefonos);        
        //CARGAR IMAGEN INICIO
        //Preguntamos si existe un archivo en el imput de name ="logo"         
        if($request->hasFile('logo')){
            //Obtenemos el nombre de la imagen con su extencion
            $filenamewithExt = $request->file('logo')->getClientOriginalName();
            //Obtenemos solo el nombre de la imagen
            $filename = pathinfo($filenamewithExt, PATHINFO_FILENAME);
            //Btenemos solo la extencion
            $extencion = $request->file('logo')->guessClientExtension();
            //File name to store
            $fileNameToStore = time().'.'.$extencion;
            //Cargamos imagen
            $path = $request->file('logo')->storeAs('public/img/empresa',$fileNameToStore);            
        }
        else{
            $fileNameToStore = "noimagen.jpg";
        }
        $empresa->emp_logo = $fileNameToStore;
        //CARGAR IMAGEN FIN         
        $empresa->emp_estado = $request->estado;       
        $empresa->save();
        return Redirect::to("empresa");
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $empresa = Empresa::findOrFail($request->emp_id);
        $empresa->emp_nombre = $request->nombre;
        $empresa->emp_codigo = $request->codigo;
        $empresa->emp_tipo_doc = $request->tipo_doc;
        $empresa->emp_num_doc = $request->num_doc;
        $empresa->emp_dircentral = $request->dircentral;
        /*INICIO EITAR TELEFONOS*/            
        if(is_array($request->telefonos)){
            $empresa->emp_telefonos = implode(',',$request->telefonos);    
        }
        else{
            if($request->telefonos == ""){                
                $empresa->emp_telefonos = null;
            }
            else{
                $empresa->emp_telefonos = $request->telefonos;    
            }            
        }     
        /*FIN EDITAR TELEFONOS*/
        /*INICIO EDITAR IMAGEN*/
        if($request->hasFile('logo'))
        {
            //Validamos si ya existe la imagen por defecto
            if($empresa->emp_logo != 'noimagen.jpg'){
                Storage::delete('public/img/empresa/'.$empresa->emp_logo);
            }
            //Obtenemos el nombre de la imagen con su extencion
            $filenamewithExt = $request->file('logo')->getClientOriginalName();
            //Obtenemos solo el nombre de la imagen
            $filename = pathinfo($filenamewithExt, PATHINFO_FILENAME);
            //Btenemos solo la extencion
            $extencion = $request->file('logo')->guessClientExtension();
            //File name to store
            $fileNameToStore = time().'.'.$extencion;
            //Cargamos imagen
            $path = $request->file('logo')->storeAs('public/img/empresa',$fileNameToStore);            
        }
        else{
            $fileNameToStore = $empresa->emp_logo;
        }               
        $empresa->emp_logo = $fileNameToStore;        
        /*FIN EDITAR IMAGEN*/        
        $empresa->emp_estado = 1;
        $empresa->save();
        return Redirect::to("empresa");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request)
    {        
        $empresa = Empresa::findOrFail($request->emp_id);
        $empresa->destroy($request->emp_id);        
        return Redirect::to("empresa");
    }
}
