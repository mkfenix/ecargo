<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacto;
use App\Cliente;
use App\Telefono;
use App\Direccion;
use App\Correo;
use App\Ciudad;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use DB;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Cliente $cliente)
    {
        $sql = trim($request -> get('buscarTexto'));
        $ciudad=DB::table('ciudad')
        // ->select('ciu_id','ciu_codigo','ciu_descripcion','pais_id')
        ->select('ciu_descripcion','pais_id')
        // CAMBIAR EL WHERE DEACUERDO A LA BASE DE DATOS Y EL CODIGO PARA BOLIVIA Y CHILE
        /*PARA BOLIVIA*/
        ->where('pais_id','=','1')
        /*PARA CHILE*/
        /*->orwhere('pais_id','=','2')*/
        ->get();
        if($request && $sql !=""){
            $idCliente = $request->idCliente;   
            $cliente = Cliente::find($idCliente);
            $contacto = $cliente->contactos()            
            ->where('cont_nombre','LIKE','%'.$sql.'%')
            ->orderBy ('cont_nombre','asc')
            ->with(['cliente','telefonos','direcciones','correos'])
            ->paginate (10);
        }
        else{            
            $idCliente = $cliente->cli_id;            
            $contacto = $cliente->contactos()
            ->where('cont_nombre','LIKE','%'.$sql.'%')
            ->orderBy ('cont_nombre','asc')
            ->with(['cliente','telefonos','direcciones','correos'])
            ->paginate (10);
        }
        return view('contacto.index',["contacto"=>$contacto,"ciudad"=>$ciudad,"buscarTexto"=>$sql, "idCliente"=>$idCliente, "cliente"=>$cliente]);
    }
    public function retornaCiudadAjax(Request $request)
    {        
            $ciudad_ajax=DB::table('ciudad')
            ->select('ciu_descripcion')
            //RECIBIMOS LOS ID DE AJAX
            ->where('pais_id','=',$request->id_bolivia)
            ->orwhere('pais_id','=',$request->id_chile)
            ->get();
            return response()->json([
                'ciudades'=>$ciudad_ajax
            ]);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacto = new Contacto();
        $contacto->cont_nombre = $request->cont_nombre;
        //SELECT TIPO DOCUMENTO SE MANEJA DE LA MISMA FORMA QUE LOS DEMAS CAMPOS
        $contacto->cont_estado = 1;
        $contacto->cli_id = $request->cli_id;
        $contacto->save();

        $x = 0;
        if($request->direcciones != null){
            foreach ($request->direcciones as $dir)
            {   
                $direccion = new Direccion();
                $direccion->dir_descripcion = $dir;
                $direccion->dir_ciudad = $request->ciudades[$x];
                $x++;
                $direccion->cont_id = $contacto->cont_id;
                $direccion->save();
            }
        }
        else{
            $mensaje = "ERROR EN LA DIRECCIÓN... NO SE GUARDO CORRECTAMENTE";
        }
        if($request->correos != null){
            foreach ($request->correos as $co)
            {   
                $correo = new Correo();
                $correo->correo_descripcion = $co;
                $correo->correo_estado = 1;
                $correo->cont_id = $contacto->cont_id;
                $correo->save();
            }
        }
        else{
            $mensaje = "ERROR EN EL CORREO... NO SE GUARDO CORRECTAMENTE";
        }
        /*PARA TELEFONO*/
        $x = 0;
        if($request->telefonos != null){
            foreach ($request->telefonos as $tel)
            {   
                $telefono = new Telefono();
                $telefono->telefono_numero = $tel;
                $telefono->telefono_tipo = $request->telefono_tipo[$x];
                $x++;
                $telefono->cont_id = $contacto->cont_id;
                $telefono->save();
            }
        }
        else{
            $mensaje = "ERROR EN EL TELÉFONO... NO SE GUARDO CORRECTAMENTE";
        }
        return Redirect::to('/cliente/'.$request->cli_id.'/contacto');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $contacto = Contacto::findOrFail($request->cont_id);
        $contacto->cont_nombre = $request->cont_nombre;
        $contacto->cont_estado = $request->cont_estado;
        $contacto->cli_id = $request->cli_id;
        $contacto->save();
        
        $x = 0;
        $contacto->direcciones()->delete();            
        if($request->direcciones != null){    
            foreach ($request->direcciones as $dir)
            {   
                $direccion = new Direccion();
                $direccion->dir_descripcion = $dir;
                $direccion->dir_ciudad = $request->ciudades[$x];
                $x++;
                $direccion->cont_id = $contacto->cont_id;
                $direccion->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UNA DIRECCION";
        }
        $contacto->correos()->delete();
        if($request->correos != null){            
            foreach ($request->correos as $co)
            {   
                $correo = new Correo();
                $correo->correo_descripcion = $co;
                $correo->correo_estado = 1;
                $correo->cont_id = $contacto->cont_id;
                $correo->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UN CORREO";
        }
        /*PARA TELEFONO*/
        $contacto->telefonos()->delete();
        $x = 0;
        if($request->telefonos != null){
            foreach ($request->telefonos as $tel)
            {   
                $telefono = new Telefono();
                $telefono->telefono_numero = $tel;
                $telefono->telefono_tipo = $request->telefono_tipo[$x];
                $x++;
                $telefono->cont_id = $contacto->cont_id;
                $telefono->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UN TELEFONO";
        }
        return Redirect::to('/cliente/'.$request->cli_id.'/contacto');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $contacto = Contacto::findOrFail($request->cont_id);
        $contacto->telefonos()->delete();
        $contacto->correos()->delete();
        $contacto->direcciones()->delete();
        $contacto->delete();
        return Redirect::to('/cliente/'.$request->cli_id.'/contacto');
    }
    public function retornaContactoAjax(Request $request)
    {        
        $idCliente = $request->id_cliente;
        $cliente = Cliente::find($idCliente);
        $contacto = $cliente->contactos()->get();
        if($contacto->isEmpty()){
            $res = 0;
            return response()->json([
                'contactos'=>$res
            ]);
        }
        else{
            $res = 1;            
            return response()->json([
                'contactos'=>$res
            ]);
        }
    }
    public function AdicionarClienteContacto(Cliente $cliente)
    {
        $contacto = new Contacto();
        $contacto->cont_nombre = $cliente->cli_nombre;
        //SELECT TIPO DOCUMENTO SE MANEJA DE LA MISMA FORMA QUE LOS DEMAS CAMPOS
        $contacto->cont_estado = 1;
        $contacto->cli_id = $cliente->cli_id;
        $contacto->save();

        $direcciones = $cliente->direcciones()->get();
        foreach ($direcciones as $dir)
        {   
            $direccion = new Direccion();
            $direccion->dir_descripcion = $dir->dir_descripcion;
            $direccion->dir_ciudad = $dir->dir_ciudad;
            $direccion->cont_id = $contacto->cont_id;
            $direccion->save();            
        }
        
        $correos = $cliente->correos()->get();
        foreach ($correos as $co)
        {   
            $correo = new Correo();
            $correo->correo_descripcion = $co->correo_descripcion;
            $correo->correo_estado = 1;
            $correo->cont_id = $contacto->cont_id;
            $correo->save();
        }
        
        $telefonos = $cliente->telefonos()->get();
        foreach ($telefonos as $tel)
        {   
            $telefono = new Telefono();
            $telefono->telefono_numero = $tel->telefono_numero;
            $telefono->telefono_tipo = $tel->telefono_tipo;            
            $telefono->cont_id = $contacto->cont_id;
            $telefono->save();
        }
        return Redirect::to('/cliente/'.$cliente->cli_id.'/contacto');
    }
}
