<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proveedor;
use App\Contacto;
use App\Telefono;
use App\Direccion;
use App\Correo;
use App\Identificacion;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ValidarFormularioProveedor;
use DB;

class ProveedorController extends Controller
{
    public function index(Request $request)
    {
        if($request){
            $sql=trim($request -> get('buscarTexto'));
            $proveedor = Proveedor::where ('prov_nombre','LIKE','%'.$sql.'%')            
            ->orderBy ('prov_nombre','asc')
            ->with(['telefonos','direcciones','correos'])
            ->paginate (10);
            // ENVIAMOS A LA VISTA LAS CIUDADES
            $ciudad=DB::table('ciudad')
            // ->select('ciu_id','ciu_codigo','ciu_descripcion','pais_id')
            ->select('ciu_descripcion','pais_id')
            /*CAMBIAR EL WHERE DEACUERDO A LA BASE DE DATOS Y EL CODIGO PARA BOLIVIA Y CHILE*/
            /*PARA BOLIVIA*/
            ->where('pais_id','=','1')
            /*PARA CHILE*/
            /*->orwhere('pais_id','=','2')*/
            ->get();

            // ENVIAMOS A LA VISTA LOS TIPOD DE DOC. IDENTIFICACION
            $identificacion=DB::table('identificacion')            
            ->select('iden_codigo')            
            ->get();
            return view('proveedor.index',[ "proveedor"         =>$proveedor,
                                            "ciudad"            =>$ciudad,
                                            "identificacion"    =>$identificacion,
                                            "buscarTexto"       =>$sql]);
        }
    }

    public function retornaCiudadAjax(Request $request)
    {        
        $ciudad_ajax=DB::table('ciudad')
        ->select('ciu_descripcion')
        //RECIBIMOS LOS ID DE AJAX
        ->where('pais_id','=',$request->id_bolivia)
        ->orwhere('pais_id','=',$request->id_chile)
        ->get();
        return response()->json([
            'ciudades'=>$ciudad_ajax
        ]);
    }   

    public function create()
    {
        // CAMBIAR PARA CIUDADES DE PROVEEDORES
        $ciudad=DB::table('ciudad')
            // ->select('ciu_id','ciu_codigo','ciu_descripcion','pais_id')
            ->select('ciu_descripcion','pais_id')
            /*CAMBIAR EL WHERE DEACUERDO A LA BASE DE DATOS Y EL CODIGO PARA BOLIVIA Y CHILE*/
            /*PARA BOLIVIA*/
            ->where('pais_id','=','1')
            /*PARA CHILE*/
            /*->orwhere('pais_id','=','2')*/
            ->get();
        $identificacion=DB::table('identificacion')            
            ->select('iden_codigo')            
            ->get();
        $direcciones = old('direcciones');
        $telefonos = old('telefonos');
        $correos = old('correos');
        return view('proveedor.form',[  'direcciones'       => $direcciones?$direcciones:[],
                                        'telefonos'         => $telefonos?$telefonos:[],
                                        'correos'           => $correos?$correos:[],
                                        'ciudad'            => $ciudad,
                                        'identificacion'    => $identificacion,
        ]);
    }

    public function store (ValidarFormularioProveedor $request)
    {
        $proveedor = new Proveedor();
        $proveedor->prov_nombre = $request->nombre;
        $proveedor->prov_tipoDoc = $request->prov_tipoDoc;
        $proveedor->prov_numDoc = $request->prov_numDoc;        
        $proveedor->save();
        
        /*PARA DIRECCIONES*/
        $x = 0;
        if($request->direcciones != null){
            foreach ($request->direcciones as $dir)
            {   
                $direccion = new Direccion();
                $direccion->dir_descripcion = $dir;
                $direccion->dir_ciudad = $request->ciudades[$x];
                $x++;
                $direccion->prov_id = $proveedor->prov_id;
                $direccion->save();
            }
        }

        /*PARA CORREOS*/
        if($request->correos != null){
            foreach ($request->correos as $co)
            {   
                $correo = new Correo();
                $correo->correo_descripcion = $co;
                $correo->correo_estado = 1;
                $correo->prov_id = $proveedor->prov_id;
                $correo->save();
            }
        }
        
        /*PARA TELEFONO*/
        $x = 0;
        if($request->telefonos != null){
            foreach ($request->telefonos as $tel)
            {   
                $telefono = new Telefono();
                $telefono->telefono_numero = $tel;
                $telefono->telefono_tipo = $request->telefono_tipo[$x];
                $x++;
                $telefono->prov_id = $proveedor->prov_id;
                $telefono->save();
            }
        }                
        
        $request->session()->flash('report','Proveedor: '.$request->nombre.' Registrado Exitosamente!!');
        return redirect()->route('proveedor.create');        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {   
        // CAMBIAR PARA CIUDADES DE PROVEEDORES
        $ciudad=DB::table('ciudad')
            // ->select('ciu_id','ciu_codigo','ciu_descripcion','pais_id')
            ->select('ciu_descripcion','pais_id')
            /*CAMBIAR EL WHERE DEACUERDO A LA BASE DE DATOS Y EL CODIGO PARA BOLIVIA Y CHILE*/
            /*PARA BOLIVIA*/
            ->where('pais_id','=','1')
            /*PARA CHILE*/
            /*->orwhere('pais_id','=','2')*/
            ->get();
        $proveedor = Proveedor::find($id);
        $identificacion=DB::table('identificacion')            
            ->select('iden_codigo')            
            ->get();

        // $direcciones = DB::table ('direccion')
        //     ->select('dir_descripcion','dir_ciudad')
        //     ->where('prov_id','=',$id)
        //     ->get();

        // $telefonos = DB::table ('telefono')
        //     ->select('telefono_tipo','telefono_numero')
        //     ->where('prov_id','=',$id)
        //     ->get();
        
        // $correos = DB::table ('correo')
        //     ->select('correo_descripcion')
        //     ->where('prov_id','=',$id)
        //     ->get();

        
        // return view('proveedor.updateForm',[    'direcciones'       => $direcciones,
        //                                         'telefonos'         => $telefonos,
        //                                         'correos'           => $correos,
        //                                         'ciudad'            => $ciudad,
        //                                         'identificacion'    => $identificacion,
        // ])->with(compact('proveedor'));
        
        
        

        
        
        if (old('direcciones') == null){
            $direcciones = DB::table ('direccion')
            ->select('dir_descripcion','dir_ciudad')
            ->where('prov_id','=',$id)
            ->get();
        }
        else{
            $oldDirecciones = old('direcciones');
            $direcciones = [];
            foreach($oldDirecciones as $indice => $valor){
                $itemDireccion = new \stdClass();
                $itemDireccion->dir_descripcion = $valor;
                $itemDireccion->dir_ciudad = old('ciudades')[$indice];
                array_push($direcciones,$itemDireccion);
            }
        }
        if(old('telefonos') == null){
            $telefonos = DB::table ('telefono')
            ->select('telefono_tipo','telefono_numero')
            ->where('prov_id','=',$id)
            ->get();
        }
        else{
            $telefonos = old('telefonos');
        }
        
        if(old('correos') == null){
            $correos = DB::table ('correo')
            ->select('correo_descripcion')
            ->where('prov_id','=',$id)
            ->get();
        }
        else{
            $correos = old('correos');
        }

        return view('proveedor.updateForm',['direcciones'       => $direcciones,
                                            'telefonos'         => $telefonos,
                                            'correos'           => $correos,
                                            'ciudad'            => $ciudad,
                                            'identificacion'    => $identificacion,
        ])->with(compact('proveedor'));

        // return view('proveedor.updateForm',['direcciones'       => $direcciones?$direcciones:[],
        //                                     'telefonos'         => $telefonos?$telefonos:[],
        //                                     'correos'           => $correos?$correos:[],
        //                                     'ciudad'            => $ciudad,
        //                                     'identificacion'    => $identificacion,
        //                                     'proveedor'         => $proveedor,
        // ]);        
    }

    public function update(ValidarFormularioProveedor $request, $id)
    {
        $proveedor = Proveedor::find($id);
        // $proveedor = Proveedor::findOrFail($request->cli_id);
        $proveedor->prov_nombre = $request->input('nombre');
        $proveedor->prov_tipoDoc = $request->input('prov_tipoDoc');
        $proveedor->prov_numDoc = $request->input('prov_numDoc');        
        $proveedor->save();

        // INICIO DIRECCIONES
        $x = 0;
        // $proveedor->direcciones()->delete();
        // if($request->direcciones != null){
        //     foreach ($request->direcciones as $dir)
        //     {   
        //         $direccion = new Direccion();
        //         $direccion->dir_descripcion = $dir;
        //         $direccion->dir_ciudad = $request->ciudades[$x];
        //         $x++;
        //         $direccion->prov_id = $proveedor->prov_id;
        //         $direccion->save();
        //     }            
        // }
        
        // INICIO CORREOS
        // $proveedor->correos()->delete();
        // if($request->correos != null){            
        //     foreach ($request->correos as $co)
        //     {   
        //         $correo = new Correo();
        //         $correo->correo_descripcion = $co;
        //         $correo->correo_estado = 1;
        //         $correo->prov_id = $proveedor->prov_id;
        //         $correo->save();
        //     }
        // }
        
        // INICIO TELEFONOS
        // $x = 0;
        // $proveedor->telefonos()->delete();
        // if($request->telefonos != null){            
        //     $x = 0;
        //     foreach ($request->telefonos as $tel)
        //     {   
        //         $telefono = new Telefono();
        //         $telefono->telefono_numero = $tel;
        //         $telefono->telefono_tipo = $request->telefono_tipo[$x];
        //         $x++;
        //         $telefono->prov_id = $proveedor->prov_id;
        //         $telefono->save();
        //     }
        // }

        // $request->session()->flash('report','Proveedor: '.$request->nombre.' Actualizado Exitosamente!!');
        // return back();
        return $request;

    }

    public function destroy(Request $request)
    {
        $proveedor = Proveedor::findOrFail($request->prov_id);        
        // foreach ($proveedor->contactos()->get() as $cont){
        //     $contacto = Contacto::findOrFail($cont->cont_id);
        //     $contacto->telefonos()->delete();
        //     $contacto->correos()->delete();
            // $contacto->direcciones()->delete();
        //     $contacto->delete();
        // }
        $proveedor->telefonos()->delete();
        $proveedor->correos()->delete();
        $proveedor->direcciones()->delete();
        $proveedor->delete();
        $request->session()->flash('report','Proveedor: '.$request->nombre.' Eliminado');
        return Redirect::to("proveedor");
    }
}
