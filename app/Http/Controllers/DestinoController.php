<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destino;
use Illuminate\Support\Facades\Redirect;
use DB;

class DestinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $destino=DB::table('destino')->where('dest_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('dest_id','desc')
            ->paginate(10);
        return view('destino.index',["destino"=>$destino,"buscarTexto"=>$sql]);
        //return $destino;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $destino= new Destino();
        $destino->dest_descripcion= $request->descripcion;
        $destino->save();
        return Redirect::to("destino");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $destino= Destino::findOrFail($request->dest_id);
        $destino->dest_descripcion= $request->descripcion;
        $destino->save();
        return Redirect::to("destino");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}

