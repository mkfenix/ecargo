<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Identificacion;
use Illuminate\Support\Facades\Redirect;
use DB;

class IdentificacionController extends Controller
{   
    public function index(Request $request)
    {
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $identificacion=DB::table('identificacion')->where('iden_descripcion','LIKE','%'.$sql.'%')
            ->orwhere('iden_codigo','LIKE','%'.$sql.'%')
            ->orderBy('iden_id','asc')
            ->paginate(10);
        return view('identificacion.index',["identificacion"=>$identificacion,"buscarTexto"=>$sql]);        
        }
    }
    
    public function store(Request $request)
    {
        //
        $identificacion= new Identificacion();
        $identificacion->iden_codigo=$request->codigo;
        $identificacion->iden_descripcion= $request->descripcion;        
        $identificacion->save();
        return Redirect::to("identificacion");
    }

    public function update(Request $request)
    {
        //
        $identificacion= Identificacion::findOrFail($request->iden_id);
        $identificacion->iden_codigo=$request->codigo;
        $identificacion->iden_descripcion= $request->descripcion;        
        $identificacion->save();
        return Redirect::to("identificacion");
    }    
}



