<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moneda;
use Illuminate\Support\Facades\Redirect;
use DB;

class MonedaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $moneda=DB::table('moneda')->where('mon_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('mon_id','desc')
            ->paginate(10);
        return view('moneda.index',["moneda"=>$moneda,"buscarTexto"=>$sql]);
        //return $moneda;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $moneda= new Moneda();
        $moneda->mon_codigo= $request->codigo;
        $moneda->mon_descripcion= $request->descripcion;
        //return $moneda;
        $moneda->save();
        return Redirect::to("moneda");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $moneda= Moneda::findOrFail($request->mon_id);
        $moneda->mon_codigo= $request->codigo;
        $moneda->mon_descripcion= $request->descripcion;
        $moneda->save();
        return Redirect::to("moneda");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
