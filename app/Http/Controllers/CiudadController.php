<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\Storage;
use DB;

class CiudadController extends Controller
{
   //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($request){
              $sql=trim($request->get('buscarTexto'));
              $ciudad=DB::table('ciudad as c')
              ->join('pais as p','p.pais_id','=','c.pais_id')
              ->select('c.ciu_id','c.ciu_codigo','c.ciu_descripcion','p.pais_descripcion as pais')
              ->where('c.ciu_descripcion','LIKE','%'.$sql.'%')
              ->orwhere('c.ciu_codigo','LIKE','%'.$sql.'%')
              ->orderBy('c.ciu_id','asc')
              ->paginate(3);
  
              $pais=DB::table('pais')
              ->select('pais_id','pais_codigo','pais_descripcion')
              ->get();
  
         return view('ciudad.index',["ciudad"=>$ciudad,"pais"=>$pais,"buscarTexto"=>$sql]);
         // return $ciudad;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ciudad= new Ciudad();
        $ciudad->ciu_codigo=$request->codigo;
        $ciudad->ciu_descripcion= $request->descripcion;
        $ciudad->pais_id=$request->pais_id;
        $ciudad->save();
        return Redirect::to("ciudad");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $ciudad= Ciudad::findOrFail($request->ciu_id);
        $ciudad->ciu_codigo=$request->codigo;
        $ciudad->ciu_descripcion= $request->descripcion;
        $ciudad->pais_id;
        $ciudad->save();
        return Redirect::to("ciudad");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}



