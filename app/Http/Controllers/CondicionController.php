<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Condicion;
use Illuminate\Support\Facades\Redirect;
use DB;


class CondicionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $condicion=DB::table('condicion')->where('cond_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('cond_id','desc')
            ->paginate(10);
        return view('condicion.index',["condicion"=>$condicion,"buscarTexto"=>$sql]);
        //return $condicion;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ciudad= new Condicion();
        $ciudad->ciu_codigo= $request->codigo;
        $ciudad->ciu_descripcion_descripcion= $request->descripcion;
        $ciudad->pa_id=$request->pais;
        $ciudad->save();
        return Redirect::to("condicion");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $ciudad= Ciudad::findOrFail($request->ciu_id);
        $ciudad->ciu_codigo= $request->codigo;
        $ciudad->ciu_descripcion= $request->descripcion;
        $ciudad->pais_id=$request->pais_id;
        $ciudad->save();
        return Redirect::to("ciudad");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}

