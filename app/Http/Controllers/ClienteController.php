<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Contacto;
use App\Telefono;
use App\Direccion;
use App\Correo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use DB;

class ClienteController extends Controller
{    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function index(Request $request)
    {        
        if($request){
            $sql=trim($request -> get('buscarTexto'));
            $cliente = Cliente::where ('cli_nombre','LIKE','%'.$sql.'%')            
            ->orderBy ('cli_nombre','asc')
            ->with(['telefonos','direcciones','correos'])
            ->paginate (10);

            $ciudad=DB::table('ciudad')
            // ->select('ciu_id','ciu_codigo','ciu_descripcion','pais_id')
            ->select('ciu_descripcion','pais_id')
            /*CAMBIAR EL WHERE DEACUERDO A LA BASE DE DATOS Y EL CODIGO PARA BOLIVIA Y CHILE*/
            /*PARA BOLIVIA*/
            ->where('pais_id','=','1')
            /*PARA CHILE*/
            /*->orwhere('pais_id','=','2')*/
            ->get();            
            return view('cliente.index',["cliente"=>$cliente,"ciudad"=>$ciudad,"buscarTexto"=>$sql]);
        }
    }

    public function retornaCiudadAjax(Request $request)
    {        
        $ciudad_ajax=DB::table('ciudad')
        ->select('ciu_descripcion')
        //RECIBIMOS LOS ID DE AJAX
        ->where('pais_id','=',$request->id_bolivia)
        ->orwhere('pais_id','=',$request->id_chile)
        ->get();
        return response()->json([
            'ciudades'=>$ciudad_ajax
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cliente = new Cliente();
        $cliente->cli_nombre = $request->nombre;
        //SELECT TIPO DOCUMENTO SE MANEJA DE LA MISMA FORMA QUE LOS DEMAS CAMPOS
        $cliente->cli_tipo_doc = $request->tipo_doc;      
        $cliente->cli_num_doc = $request->num_doc;
        $cliente->cli_ci = $request->ci;
        $cliente->cli_tipo = $request->cli_tipo;
        $cliente->save();        

        $x = 0;
        if($request->direcciones != null){
            foreach ($request->direcciones as $dir)
            {   
                $direccion = new Direccion();
                $direccion->dir_descripcion = $dir;
                $direccion->dir_ciudad = $request->ciudades[$x];
                $x++;
                $direccion->cli_id = $cliente->cli_id;
                $direccion->save();
            }
        }
        else{
            $mensaje = "ERROR EN LA DIRECCIÓN... NO SE GUARDO CORRECTAMENTE";
        }
        if($request->correos != null){
            foreach ($request->correos as $co)
            {   
                $correo = new Correo();
                $correo->correo_descripcion = $co;
                $correo->correo_estado = 1;
                $correo->cli_id = $cliente->cli_id;
                $correo->save();
            }
        }
        else{
            $mensaje = "ERROR EN EL CORREO... NO SE GUARDO CORRECTAMENTE";
        }
        /*PARA TELEFONO*/
        $x = 0;
        if($request->telefonos != null){
            foreach ($request->telefonos as $tel)
            {   
                $telefono = new Telefono();
                $telefono->telefono_numero = $tel;
                $telefono->telefono_tipo = $request->telefono_tipo[$x];
                $x++;
                $telefono->cli_id = $cliente->cli_id;
                $telefono->save();
            }
        }
        else{
            $mensaje = "ERROR EN EL TELÉFONO... NO SE GUARDO CORRECTAMENTE";
        }
        return Redirect::to("cliente");        
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $cliente = Cliente::findOrFail($request->cli_id);
        $cliente->cli_nombre = $request->nombre;
        $cliente->cli_tipo_doc = $request->tipo_doc;
        $cliente->cli_num_doc = $request->num_doc;
        $cliente->cli_ci = $request->ci;
        $cliente->save();

        $x = 0;
        $cliente->direcciones()->delete();
        if($request->direcciones != null){
            foreach ($request->direcciones as $dir)
            {   
                $direccion = new Direccion();
                $direccion->dir_descripcion = $dir;
                $direccion->dir_ciudad = $request->ciudades[$x];
                $x++;
                $direccion->cli_id = $cliente->cli_id;
                $direccion->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UNA DIRECCION";
        }
        $cliente->correos()->delete();
        if($request->correos != null){            
            foreach ($request->correos as $co)
            {   
                $correo = new Correo();
                $correo->correo_descripcion = $co;
                $correo->correo_estado = 1;
                $correo->cli_id = $cliente->cli_id;
                $correo->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UN CORREO";
        }
        /*PARA TELEFONO*/
        $x = 0;
        $cliente->telefonos()->delete();
        if($request->telefonos != null){            
            $x = 0;
            foreach ($request->telefonos as $tel)
            {   
                $telefono = new Telefono();
                $telefono->telefono_numero = $tel;
                $telefono->telefono_tipo = $request->telefono_tipo[$x];
                $x++;
                $telefono->cli_id = $cliente->cli_id;
                $telefono->save();
            }
        }
        else{
            $mensaje = "DEBE AÑADIR UN TELEFONO";
        }        
        return Redirect::to("cliente");
        // return view("cliente")->with('mensaje',$mensaje);
        // return $mensaje;
        // return redirect()->action('ClienteController@index', $mensajes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */    

    public function destroy(Request $request)
    {
        $cliente = Cliente::findOrFail($request->cli_id);
        // $x = $cliente->contactos()->get();
        foreach ($cliente->contactos()->get() as $cont){
            $contacto = Contacto::findOrFail($cont->cont_id);
            $contacto->telefonos()->delete();
            $contacto->correos()->delete();
            $contacto->direcciones()->delete();
            $contacto->delete();
        }
        $cliente->telefonos()->delete();
        $cliente->correos()->delete();
        $cliente->direcciones()->delete();
        $cliente->delete();
        return Redirect::to("cliente");
    }

}
