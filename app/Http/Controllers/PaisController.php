<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pais;
use Illuminate\Support\Facades\Redirect;
use DB;


class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $pais=DB::table('pais')->where('pais_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('pais_id','desc')
            ->paginate(2);
        return view('pais.index',["pais"=>$pais,"buscarTexto"=>$sql]);
        //return $pais;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $pais= new Pais();
        $pais->pais_codigo= $request->codigo;
        $pais->pais_descripcion= $request->descripcion;
        //return $pais;
        $pais->save();
        return Redirect::to("pais");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $pais= Pais::findOrFail($request->pais_id);
        $pais->pais_codigo= $request->codigo;
        $pais->pais_descripcion= $request->descripcion;
        $pais->save();
        return Redirect::to("pais");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
