<?php

namespace App\Http\Controllers;
use App\Emision;
use Illuminate\Support\Facades\Redirect;
use DB;

use Illuminate\Http\Request;

class EmisionController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $emision=DB::table('emision')->where('emi_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('emi_id','asc')
            ->paginate(10);
        return view('emision.index',["emision"=>$emision,"buscarTexto"=>$sql]);
        //return $emision;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $emision= new Emision();
        $emision->emi_descripcion= $request->descripcion;
        $emision->save();
        return Redirect::to("emision");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $emision= Emision::findOrFail($request->emi_id);
        $emision->emi_descripcion= $request->descripcion;
        $emision->save();
        return Redirect::to("emision");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}



