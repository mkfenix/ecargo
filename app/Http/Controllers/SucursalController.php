<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Sucursal;
use App\EmpresaSucursal;
use App\Direccion;
use DB;


class SucursalController extends Controller
{
    //
    public function index(Request $request)
    {
        //
        if($request){ 

            $sql=trim($request->get('buscarTexto'));
              $sucursal=DB::table('sucursal as S')
              ->join('empresa_sucursal as ES','S.suc_id','=','ES.suc_id')
              ->join('empresa as E','E.emp_id','=','ES.emp_id')
              ->join('direccion as D', 'D.suc_id','=','S.suc_id')
              ->select('S.suc_id','S.suc_ciudad','E.emp_nombre as empresa','D.dir_descripcion as descripcion')     
              ->where('S.suc_ciudad','LIKE','%'.$sql.'%')
              ->orwhere('E.emp_nombre','LIKE','%'.$sql.'%')
              ->orderBy('S.suc_ciudad','asc')
              ->paginate(6);

            $empresa=DB::table('empresa')
            ->select('emp_id','emp_nombre')
            ->get();

            $empresa_sucursal=DB::table('empresa_sucursal')
            ->select('emp_id','suc_id')
            ->get();

            $direccion = DB::table('direccion')
            ->select ('dir_id','dir_descripcion')
            ->get();

            return view('sucursal.index',["sucursal"=>$sucursal,"empresa"=>$empresa,"buscarTexto"=>$sql]);                
        }
    }

    public function store(Request $request)
    {
        $sucursal = new Sucursal();
        $sucursal->suc_ciudad=$request->ciudad;
        $sucursal->suc_estado= $request->estado;        
        //$sucursal->save();

        $sucursalempresa = new EmpresaSucursal();
        //$sucursalempresa->emp_id=$request->emp_id;
        //$sucursalempresa->suc_id= $request->suc_id;        
        //$sucursalempresa->save();

        $direccion = new Direccion();
        //$direccion 

        ['dir_descripcion','suc_id','em_id','em_id','prov_id','cli_id','nav_id'];


        return Redirect::to("sucursal");
    }

    public function update(Request $request)
    {
        $ciudad= Ciudad::findOrFail($request->ciu_id);
        $ciudad->ciu_codigo=$request->codigo;
        $ciudad->ciu_descripcion= $request->descripcion;
        $ciudad->pais_id;
        $ciudad->save();
        return Redirect::to("ciudad");
    }

    public function destroy(Request $request)
    {
        $empresa = Empresa::findOrFail($request->emp_id);
        $empresa->destroy($request->emp_id);        
        return Redirect::to("empresa");    	
    }
}
