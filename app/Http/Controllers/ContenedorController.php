<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contenedor;
use Illuminate\Support\Facades\Redirect;
use DB;

class ContenedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request){
            $sql=trim($request->get('buscarTexto'));
            $contenedor=DB::table('contenedor')->where('cont_descripcion','LIKE','%'.$sql.'%')
            ->orderBy('cont_id','desc')
            ->paginate(10);
        return view('contenedor.index',["contenedor"=>$contenedor,"buscarTexto"=>$sql]);
        //return $contenedor;
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $contenedor= new Contenedor();
        $contenedor->cont_tamano= $request->tamano;
        $contenedor->cont_tipo= $request->tipo;
        $contenedor->cont_descripcion= $request->descripcion;
        $contenedor->save();
        return Redirect::to("contenedor");


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $contenedor= Contenedor::findOrFail($request->cont_id);
        $contenedor->cont_tamano= $request->tamano;
        $contenedor->cont_tipo= $request->tipo;
        $contenedor->cont_descripcion= $request->descripcion;
        $contenedor->save();
        return Redirect::to("contenedor");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}

