<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'telefono';
    protected $fillable = ['telefono_numero','telefono_tipo','cli_id','nav_id','cont_id','em_id','prov_id'];
    public $timestamps = false;
    protected $primaryKey = 'telefono_id';
	
	public function cliente()
	{
	    return $this->belongsTo(Cliente::class,'cli_id');
	}
}
