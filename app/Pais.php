<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    //
    protected $table = 'pais';
    protected $fillable=['pais_codigo, pais_descripcion'];
    public $timestamps = false;
    protected $primaryKey = 'pais_id';

}   
