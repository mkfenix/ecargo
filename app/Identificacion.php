<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Identificacion extends Model
{
	protected $table = 'identificacion';
    protected $fillable = ['iden_codigo','iden_descripcion'];
    public $timestamps = false;
    protected $primaryKey = 'iden_id';	
}
