<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    //
    protected $table='direccion';
    protected $fillable=['dir_descripcion','dir_ciudad','em_id','prov_id','cli_id','nav_id'];
    public $timestamps=false;
    protected $primaryKey='dir_id';

    public function cliente()
	{
	    return $this->belongsTo(Cliente::class,'cli_id');
	}
}