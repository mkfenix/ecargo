<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpresaSucursal extends Model
{
    //
    protected $table='empresa_sucursal';
    protected $fillable=['emp_id','suc_id'];
    public $timestamps=false;
    protected $primaryKey=['emp_id','suc_id'];    
}