<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    //
    protected $table='ciudad';
    protected $fillable=['ciu_codigo','ciu_descripcion','pais_id'];
    public $timestamps=false;
    protected $primaryKey='ciu_id';
}
