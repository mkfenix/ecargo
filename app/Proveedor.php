<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedor';
    protected $fillable = ['prov_nombre','prov_tipoDoc','prov_numDoc'];
    public $timestamps = false;
    protected $primaryKey = 'prov_id';
	
	public function telefonos()
	{
	    return $this->hasMany(Telefono::class,'prov_id');
	}

	public function direcciones()
	{
	    return $this->hasMany(Direccion::class,'prov_id');
	}

	public function correos()
	{
	    return $this->hasMany(Correo::class,'prov_id');
	}

	public function contactos()
	{
	    return $this->hasMany(Contacto::class,'prov_id');
	}
	public function identificacion()
	{
	    return $this->hasMany(Identificacion::class,'prov_id');
	}

}
