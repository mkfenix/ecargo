//INICIO SCRIPT EMPRESA
//INICIO INSERTA Y ELIMINAR FILAS DE TELEFONOS SOLO PARA EMPRESA    
var maxFilas = 4; // Determina el Maximo de filas a generar
var filas = 0; //Iniciamos el contador de filas en 1          
var addButton = $('.add_button'); // Para llamar a la clase add_button
var wrapperTabla = $('.tabla_wrapper'); // Para llamar a la clase tabla_wrapper (envoltorio de la tabla)
var nuevaFila = '<tr class="fila"><td><input type="text" name ="telefonos[]" class="form-control" placeholder="Telefono(s)" required required pattern="[0-9+()-]+"/></td><td><a href="javascript:void(1);" class="eliminar" title="Remove field"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosTelefonosEmpresa();" style="margin-left: 15px;"></a></td></tr>'; //La nueva Fila.
var vectelefonos = [];
//Detectamos el tamaño del vector "vector_telefonos" par generar las filas correctas de telefonos en Editar telefono 
$('#abrirmodalEditarEmpresa').on('show.bs.modal', function (event){
  var boton = $(event.relatedTarget)
  var telefonos_modal = boton.data('emp_telefonos')
  if(telefonos_modal.toString().indexOf(",") != -1){
    //Para mas de un valor
    vectelefonos = telefonos_modal.split(',');
  }
  else{
    //Para un solo valor
    vectelefonos = [telefonos_modal];          
  }
  filas = filas + vectelefonos.length;                        
});

function masTelefonosEmpresa(){      
  if(vectelefonos.length == 1 && vectelefonos[0] ==""){
    if(filas <= maxFilas){          
      $(wrapperTabla).append(nuevaFila);            
      filas ++;
    }  
  }
  else{
    if(filas < maxFilas){          
      $(wrapperTabla).append(nuevaFila);
      filas ++;
    }
  }
}
$('#abrirmodalEmpresa').on('hidden.bs.modal', function (){
  $('.fila').remove();
  filas = 0;          
})
$('#abrirmodalEditarEmpresa').on('hidden.bs.modal', function (){
  $('.fila').remove();
  filas = 0;          
})     
function menosTelefonosEmpresa(){      
  $(wrapperTabla).on("click", ".eliminar", function(e){
    e.preventDefault();
    $(this).parents('tr').remove();
  });
  filas --;                      
}

//FIN INSERTA Y ELIMINAR FILAS DE TELEFONOS SOLO PARA EMPRESA
//INICIO EDITAR EMPRESA

$('#abrirmodalEditarEmpresa').on('show.bs.modal', function (event){
  var button = $(event.relatedTarget)   
  var nombre_modal_editar = button.data('emp_nombre')
  var codigo_modal_editar = button.data('emp_codigo')
  var tipo_doc_modal_editar = button.data('emp_tipo_doc')
  var num_doc_modal_editar = button.data('emp_num_doc')
  var dircentral_modal_editar = button.data('emp_dircentral')
  var telefonos_modal_editar = button.data('emp_telefonos')
  var estado_modal_editar = button.data('emp_estado')        
  var emp_id = button.data('emp_id')
  var modal = $(this)   
   //modal.find('.modal-title').text('New message to ' + recipient)
  modal.find('.modal-body #emp_nombre').val(nombre_modal_editar);
  modal.find('.modal-body #emp_codigo').val(codigo_modal_editar);
  modal.find('.modal-body #emp_tipo_doc').val(tipo_doc_modal_editar);
  modal.find('.modal-body #emp_num_doc').val(num_doc_modal_editar);
  modal.find('.modal-body #emp_dircentral').val(dircentral_modal_editar);
  //INICIO EDITAR TELEFONOS; telefonos modal_editar llega como un STRING
  var wrapperTabla = $('.tabla_wrapper');//var wrapperTabla = $('.tabla_wrapper'); // Para llamar a la clase tabla_wrapper (envoltorio de la tabla)
  var nuevaFila = "";
  //Preguntamos si existen "," en variable telefonos_modal_editar       
  if(telefonos_modal_editar.toString().indexOf(",") != -1){
    //Para mas de un valor
    var vector_telefonos = telefonos_modal_editar.split(',');          
  }
  else{
    //Para un solo valor
    var vector_telefonos = [telefonos_modal_editar];
  }        
  if(vector_telefonos != ""){
    for (var i = 0; i <= vector_telefonos.length-1; i++){            
      nuevaFila = '<tr class="fila"><td><input type="text" name ="telefonos[]" class="form-control col-md-3" placeholder="Telefono(s)" value="'+vector_telefonos[i]+'"required required pattern="[0-9+()-]+"/></td><td><a href="javascript:void(1);" class="eliminar" title="Remove field"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosTelefonosEmpresa();" style="margin-left: 15px;"></a></td></tr>'; //La nueva Fila
        //Funcion adicionar filas
      $(wrapperTabla).append(nuevaFila);
    }  
  }         
  $('#abrirmodalEditarEmpresa').on('hidden.bs.modal', function () {
    $('.fila').remove();              
  })        
  //FIN EDITAR TELEFONOS
  modal.find('.modal-body #emp_estado').val(estado_modal_editar);
  modal.find('.modal-body #emp_id').val(emp_id);                   
})

//FIN EDITAR EMPRESA
//INICIO MODAL PARA CONFIRMACION DE ELIMINAR EMPRESA

$('#abrirmodalEliminarEmpresa').on('shown.bs.modal', function(event){
  var button = $(event.relatedTarget)
  var emp_id = button.data('emp_id')
  var modal = $(this)
  modal.find('.modal-body #emp_id').val(emp_id);
})

//FIN MODAL PARA CONFIRMACION DE ELIMINAR EMPRESA
//FIN SCRIPT EMPRESA