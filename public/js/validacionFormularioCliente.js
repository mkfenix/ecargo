$('#abrirmodalEditarCliente').on('show.bs.modal', function (){    
    $('#formularioEditarCliente').bootstrapValidator({
        message: 'Este valor no es válido.',
        feedbackIcons: {
            // valid: 'glyphicon glyphicon-ok',
            // invalid: 'glyphicon glyphicon-remove',
            // validating: 'glyphicon glyphicon-refresh'

            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        excluded: [':disabled'],
        fields: {            
            nombre: {
                message: 'El nombre no es valido.',
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vacio.'
                    },
                    stringLength: {
                        min: 5,
                        max: 50,
                        message: 'El nombre debe tener entre 5 y 50 caracteres de largo.'
                    }
                }            
            },
            ci: {
                validators: {
                    notEmpty: {
                        message: 'El Carnet no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'El carnet solo puede tener caracteres alphanumericos.'
                    }                    
                }
            },
            tipo_doc: {
                validators: {
                    notEmpty: {
                        message: 'Debe escoger una opcion.'
                    }
                }
            },
            num_doc: {
                validators: {
                    notEmpty: {
                        message: 'El número de documento no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'El número de documento solo puede tener caracteres alphanumericos.'
                    }                    
                }
            },
            'direcciones[]': {
                validators: {
                    notEmpty: {
                        message: 'La direccion no puede estar vacia.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'La direccion no puede tener caracteres especiales.'
                    },
                    stringLength: {
                        min: 5,                        
                        message: 'La dirección debe tener más 5 caracteres de largo.'
                    }                                        
                }
            },
            'telefonos[]': {              
                validators: {
                    notEmpty: {
                        message: 'El teléfono no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[0-9+()-]+$/,                        
                        message: 'El teléfono no puede tener caracteres especiales ni letras.'
                    },
                }
            },
            'correos[]': {
                validators: {
                    notEmpty: {
                        message: 'El correo no puede estar vacio.'
                    },
                    emailAddress: {
                        message: 'No es un correo válido.'
                    }                                        
                }
            },            
        }
    });
});
$('#abrirmodalEditarCliente').on('hidden.bs.modal', function (){    
    $('#formularioEditarCliente').bootstrapValidator("resetForm",true);    
});


$('#abrirmodalCliente').on('show.bs.modal', function (){    
    $('#formularioAgregarCliente').bootstrapValidator({
        message: 'Este valor no es válido.',
        feedbackIcons: {
            // valid: 'glyphicon glyphicon-ok',
            // invalid: 'glyphicon glyphicon-remove',
            // validating: 'glyphicon glyphicon-refresh'

            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        excluded: [':disabled'],
        fields: {
            nombre: {
                message: 'El nombre no es valido.',
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vacio.'
                    },
                    stringLength: {
                        min: 5,
                        max: 50,
                        message: 'El nombre debe tener entre 5 y 50 caracteres de largo.'
                    }
                }            
            },
            ci: {
                validators: {
                    notEmpty: {
                        message: 'El Carnet no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'El carnet solo puede tener caracteres alphanumericos.'
                    }                    
                }
            },
            tipo_doc: {
                validators: {
                    notEmpty: {
                        message: 'Debe escoger una opcion.'
                    }
                }
            },
            num_doc: {
                validators: {
                    notEmpty: {
                        message: 'El número de documento no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'El número de documento solo puede tener caracteres alphanumericos.'
                    }                    
                }
            },
            'direcciones[]': {
                validators: {
                    notEmpty: {
                        message: 'La direccion no puede estar vacia.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'La direccion no puede tener caracteres especiales.'
                    },
                    stringLength: {
                        min: 5,                        
                        message: 'La dirección debe tener más 5 caracteres de largo.'
                    }                                        
                }
            },            
            'telefonos[]': {              
                validators: {
                    notEmpty: {
                        message: 'El teléfono no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[0-9+()-]+$/,                        
                        message: 'El teléfono no puede tener caracteres especiales ni letras.'
                    },
                }
            },
            'correos[]': {
                validators: {
                    notEmpty: {
                        message: 'El correo no puede estar vacio.'
                    },
                    emailAddress: {
                        message: 'No es un correo válido.'
                    }                                        
                }
            },           
        }
    });
});
$('#abrirmodalCliente').on('hidden.bs.modal', function (){    
    $('#formularioAgregarCliente').bootstrapValidator("resetForm",true);    
});
