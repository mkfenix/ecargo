// INICIO CLONAR IMPUTS
$(document).ready(function(){
    MAX_FILAS = 4;
    // var numFilasDirecciones = $('input[name="direcciones[]"]').length;
    // var numFilasTelefonos = $('input[name="telefonos[]"]').length;
    // var numFilasCorreos = $('input[name="correos[]"]').length;
    // CONTADORES PARA SABER CUANTAS VECES SE PRESIONA EL BOTON ELIMINAR X PARA DIRECCIONES, Y PARA TELEFONOS, Z PAA CORREOS
    x=0;
    y=0;
    z=0;    
    $('.addButton').on('click', function() {        
        // INICIA EL INDICE DE CADA ELEMENTO A CLONAR
        var index = $(this).data('index');
        if (!index) {
            index = 1;
            $(this).data('index', 1);
        }
        var numFilasDirecciones = $('input[name="direcciones[]"]').length;        
        var numFilasTelefonos = $('input[name="telefonos[]"]').length;
        var numFilasCorreos = $('input[name="correos[]"]').length;
        // OBTENEMOS EL TEMPLATE DEL BOTON ADIOCIONAR DEL FORMULARIO
        var btn_adicionar = $(this).attr('data-template');
        // CALCULAMOS EL INDICE DEL NUEVO ELEMENTO CLONADO A PARTIR DE LAS VECES QUE SE PRESIONO BOTON ELIMINAR
        if(btn_adicionar == "direcciones"){            
            index = index - x;
            x=0;            
        }
        if(btn_adicionar == "telefonos"){
            index = index - y;
            y=0;
        }
        if(btn_adicionar == "correos"){
            index = index - z;
            z=0;
        }
        index++;        
        if(index <= MAX_FILAS){
            // ASIGNAMOS INDICE Y CLONAMOS
            $(this).data('index', index);

            var template     = $(this).attr('data-template'),
                $templateEle = $('#' + template + 'Template'),
                $row         = $templateEle.clone().removeAttr('id').insertBefore($templateEle).removeClass('hide'),
                $el          = $row.find('input').eq(0).attr('name', template + '[]');
            
            // GENERA LOS ID DE CADA NUEVO IMPUT CON LA VARIABLE INDEX
            if(template == "direcciones"){            
                $el.attr('id', "idDireccion" + index);                
            }
            if(template == "telefonos"){
                $el.attr('id', "idTelefono" + index);
            }
            if(template == "correos"){
                $el.attr('id', "idCorreo" + index);
            }
            // PONE EN EL PLACEHOLDER EL INDICE DE CADA IMPUT
            if ($el.attr('id') == "idDireccion" + index) {
                $el.attr('placeholder', 'Dirección #' + index);    
            }
            if ($el.attr('id') == "idTelefono" + index) {
                $el.attr('placeholder', 'Teléfono #' + index);    
            }
            if ($el.attr('id') == "idCorreo" + index) {
                $el.attr('placeholder', 'Correo #' + index);    
            }
        }
        // ELIMINA EL IMPUT CLONADO
        $row.on('click', '.removeButton', function(e){
            var btn_eliminar = $(this).attr('data-template');
            if(btn_eliminar == "direcciones"){                
                x++;
            }
            if(btn_eliminar == "telefonos"){                
                y++;
            }
            if(btn_eliminar == "correos"){                
                z++;
            }           
            $row.remove();
        });        
    });
});
// FIN CLONAR IMPUTS

// INICIO SCRIPT PARA MODAL ELIMINAR PROVEEDOR
$('#abrirmodalEliminarProveedor').on('shown.bs.modal', function(event){
  var button = $(event.relatedTarget)
  var prov_id = button.data('prov_id')
  var modal = $(this)
  modal.find('.modal-body #prov_id').val(prov_id);
})
// FIN SCRIPT PARA MODAL ELIMINAR PROVEEDOR