function ocultarCamposContacto(){        
	document.getElementById('estadoCliente').style.display='none';          
}
function mostrarCamposContacto(){        
	document.getElementById('estadoCliente').style.display='';          
}
// INICIO INSERTA Y ELIMINAR FILAS DE TELEFONOS USANDO VECTOR
$('#abrirmodalEditarContacto').on('show.bs.modal', function (event){
	var botonTelefono = $(event.relatedTarget)
	var jsonTelefonos = botonTelefono.data('telefonos')
	filasTelefono = filasTelefono + Object.keys(jsonTelefonos).length;        
})            
$('#abrirmodalContacto').on('hidden.bs.modal', function (){
	$('.filaTelefono').remove();
	filasTelefono = 0;
})
$('#abrirmodalEditarContacto').on('hidden.bs.modal', function (){
	$('.filaTelefono').remove();
	filasTelefono = 0;
})
// FIN INSERTA Y ELIMINAR FILAS DE TELEFONOS USANDO VECTOR

// INICIO INSERTA Y ELIMINAR FILAS DE DIRECCIONES
$('#abrirmodalEditarContacto').on('show.bs.modal', function (event){
	var botonDireccion = $(event.relatedTarget)
	var jsonDireccion = botonDireccion.data('direcciones')
	filasDireccion = filasDireccion + Object.keys(jsonDireccion).length;
})      
$('#abrirmodalContacto').on('hidden.bs.modal', function (){
	$('.filaDireccion').remove();
	filasDireccion = 0;
})
$('#abrirmodalEditarContacto').on('hidden.bs.modal', function (){
	$('.filaDireccion').remove();
	filasDireccion = 0;
})
// FIN INSERTA Y ELIMINAR FILAS DE DIRECCIONES

// INICIO INSERTA Y ELIMINAR FILAS DE CORREOS
$('#abrirmodalEditarContacto').on('show.bs.modal', function (event){
  var botonCorreo = $(event.relatedTarget)
  var jsonCorreo = botonCorreo.data('correos')
  filasCorreo = filasCorreo + Object.keys(jsonCorreo).length;  
})
$('#abrirmodalContacto').on('hidden.bs.modal', function (){
    $('.filaCorreo').remove();
    filasCorreo = 0;
})
$('#abrirmodalEditarContacto').on('hidden.bs.modal', function (){
	$('.filaCorreo').remove();
    filasCorreo = 0;
})
// FIN INSERTA Y ELIMINAR FILAS DE CORREOS

// INICIO EDITAR CONTACTO
$('#abrirmodalEditarContacto').on('show.bs.modal', function (event){        
    var button = $(event.relatedTarget);
    var nombre_modal_editar = button.data('cont_nombre')
    var jsonTelefonos_modal_editar = button.data('telefonos')
    var jsonDirecciones_modal_editar = button.data('direcciones')
    var jsonCiudades_modal_editar = button.data('ciudades')
    var jsonCorreos_modal_editar = button.data('correos')    
    var cli_id = button.data('cli_id')
    var cont_id = button.data('cont_id')
    var modal = $(this)   
     //modal.find('.modal-title').text('New message to ' + recipient)
    modal.find('.modal-body #cont_nombre').val(nombre_modal_editar);
    modal.find('.modal-body #cli_id').val(cli_id);
    modal.find('.modal-body #cont_id').val(cont_id);
 
    //INICIO EDITAR TELEFONOS; jsonTelefonos_modal_editar llega como un JSON
    var wrapperTablaTelefono = $('.tabla_wrapperTelefono');// Para llamar a la clase tabla_wrapper (envoltorio de la tabla)
    var nuevaFilaTelefono = "";
    var selectTipoTelefono ="";
    var tipoTelefono="";
    if (jsonTelefonos_modal_editar!=""){          
      $.each(jsonTelefonos_modal_editar, function(i,item){
        if(jsonTelefonos_modal_editar[i].telefono_tipo === "Celular"){
          selectTipoTelefono = '<select name="telefono_tipo[]" id="telefono_tipo" class="form-control" style="width:130px;"><option value="Celular" selected id="celular">Celular</option><option value="Fijo" id="fijo">Fijo</option><option value="Fax" id="fax">Fax</option></select>';              
        }
        if(jsonTelefonos_modal_editar[i].telefono_tipo === "Fijo"){
          selectTipoTelefono = '<select name="telefono_tipo[]" id="telefono_tipo" class="form-control" style="width:130px;"><option value="Celular" id="celular">Celular</option><option value="Fijo" selected id="fijo">Fijo</option><option value="Fax" id="fax">Fax</option></select>';           
        }
        if(jsonTelefonos_modal_editar[i].telefono_tipo === "Fax"){
          selectTipoTelefono = '<select name="telefono_tipo[]" id="telefono_tipo" class="form-control" style="width:130px;"><option value="Celular" id="celular">Celular</option><option value="Fijo" id="fijo">Fijo</option><option value="Fax" selected id="fax">Fax</option></select>';
        }            
        // FILA PARCIAL
        filaParcialTelefono = '</td><td><input type="text" name ="telefonos[]" class="form-control" placeholder="Telefono(s)" required required pattern="[0-9+()-]+" value="'+jsonTelefonos_modal_editar[i].telefono_numero+'"style="width:200px;"/></td><td><a href="javascript:void(1);" class="eliminarTelefono" title="Remove field"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosTelefonos();" style="margin-left: 15px;"></a></td></tr>';
        nuevaFilaTelefono ='<tr class="filaTelefono"><td>'+selectTipoTelefono+filaParcialTelefono;
        $(wrapperTablaTelefono).append(nuevaFilaTelefono);            
      })  
    }       
    $('#abrirmodalEditarContacto').on('hidden.bs.modal', function () {
      $('.filaTelefono').remove();              
    })        
    //FIN EDITAR TELEFONOS

    //INICIO EDITAR DIRECCIONES; jsonDirecciones_modal_editar llega como un JSON
    //ARMAMOS LOS SELECT CON LAS CUIDADES       
    var optionCiudad = "";
    var wrapperTablaDireccion = $('.tabla_wrapperDireccion');// Para llamar a la clase tabla_wrapper (envoltorio de la tabla)
    var nuevaFilaDireccion = "";
    if (jsonDirecciones_modal_editar!=""){
      $.each(jsonDirecciones_modal_editar, function(i,item){            
        // La nueva Fila
        nuevaFilaDireccion = '<tr class="filaDireccion"><td><select name="ciudades[]" id="dir_ciudad" class="form-control dir_ciudad" style="width:130px;"><option value="'+jsonDirecciones_modal_editar[i].dir_ciudad+'">'+jsonDirecciones_modal_editar[i].dir_ciudad+'</option></select></td><td><input type="text" name ="direcciones[]" class="form-control" placeholder="Direccion(es)" value="'+jsonDirecciones_modal_editar[i].dir_descripcion+'"style="width:400px;"/></td><td><a href="javascript:void(1);" class="eliminarDireccion" title="Quitar Direccion"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosDirecciones();" style="margin-left: 15px;"></a></td></tr>';
        //Funcion adicionar filas
        $(wrapperTablaDireccion).append(nuevaFilaDireccion);            
      })  
    }
    // LLENAMOS LOS OPTION CON JSON CIUDADES Y LAS AÑADIMOS AL PADRE SELECT
    $.each(jsonCiudades_modal_editar, function(i,item){ 
      optionCiudad = '<option value="'+jsonCiudades_modal_editar[i].ciu_descripcion+'">'+jsonCiudades_modal_editar[i].ciu_descripcion+'</option>';
      $('.dir_ciudad').append(optionCiudad);
    })
    $('#abrirmodalEditarContacto').on('hidden.bs.modal', function () {
      $('.filaDireccion').remove();              
    })
    //INICIO PARA GENERAR NUEVAS FILAS DE DIRECCIONES CON EL SELECT CIUDADES CARGADO
    var maxFilasDireccion = 4; // Determina el Maximo de filas a generar
    var filasDireccion = 0; //Iniciamos el contador de filas en 1          
    var addButtonDireccion = $('.add_buttonDireccion'); // Para llamar a la clase add_button
    var wrapperTablaDireccion = $('.tabla_wrapperDireccion'); // Para llamar a la clase      
    //GENERAMOS LA FILA DE FORMPA PARCIAL
    var parcialFilaDireccion = '</td><td><input type="text" name ="direcciones[]" class="form-control" placeholder="Direccion(es)"style="width:400px;"/></td><td><a href="javascript:void(1);" class="eliminarDireccion" title="Remove field"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosDirecciones();" style="margin-left: 15px;"></a></td></tr>';
    var ciudades = [];
    $('#abrirmodalEditarContacto').on('show.bs.modal', function (event){
      var botonDireccion = $(event.relatedTarget)
      var jsonDireccion = botonDireccion.data('direcciones')
      filasDireccion = filasDireccion + Object.keys(jsonDireccion).length;
    })
    $('#abrirmodalContacto').on('hidden.bs.modal', function (){
      $('.filaDireccion').remove();
      filasDireccion = 0;       
    })
    $('#abrirmodalEditarContacto').on('hidden.bs.modal', function (){
      $('.filaDireccion').remove();
      filasDireccion = 0;
    })
    //INICIO EDITAR CORREOS; jsonCorreos_modal_editar llega como un JSON    
    var wrapperTablaCorreo = $('.tabla_wrapperCorreo');// Para llamar a la clase tabla_wrapper (envoltorio de la tabla)
    var nuevaFilaCorreo = "";
    if (jsonCorreos_modal_editar!=""){
      $.each(jsonCorreos_modal_editar, function(i,item){            
        // La nueva Fila
        nuevaFilaCorreo = '<tr class="filaCorreo"><td><input type="email" name ="correos[]" class="form-control" placeholder="Correo(s)" value="'+jsonCorreos_modal_editar[i].correo_descripcion+'"style="width:200px;"/></td><td><a href="javascript:void(1);" class="eliminarCorreo" title="Quitar Correo"><input type="button" value="Quitar -" class="btn btn-warning" onclick="menosCorreos();" style="margin-left: 15px;"></a></td></tr>';
        //Funcion adicionar filas
        $(wrapperTablaCorreo).append(nuevaFilaCorreo);
      })  
    }           
    $('#abrirmodalEditarContacto').on('hidden.bs.modal', function () {
      $('.filaCorreo').remove();      
    })
    //FIN EDITAR CORREOS
})
// FIN EDITAR CONTACTO
// INICIO MODAL PARA CONFIRMACION DE ELIMINAR CONTACTO         
$('#abrirmodalEliminarContacto').on('shown.bs.modal', function(event){
  var button = $(event.relatedTarget)
  var cont_id = button.data('cont_id')
  var cli_id = button.data('cli_id')
  var modal = $(this)
  modal.find('.modal-body #cont_id').val(cont_id);
  modal.find('.modal-body #cli_id').val(cli_id);
})
// FIN MODAL PARA CONFIRMACION DE ELIMINAR CONTACTO


// INICIO VALIDACION DE FORMULARIOS
$('#abrirmodalContacto').on('show.bs.modal', function (){    
    $('#formularioAgregarContacto').bootstrapValidator({
        message: 'Este valor no es válido.',
        feedbackIcons: {
            // valid: 'glyphicon glyphicon-ok',
            // invalid: 'glyphicon glyphicon-remove',
            // validating: 'glyphicon glyphicon-refresh'
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        excluded: [':disabled'],
        fields: {            
            cont_nombre: {
                message: 'El nombre no es valido.',
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vacio.'
                    },
                    stringLength: {
                        min: 5,
                        max: 50,
                        message: 'El nombre debe tener entre 5 y 50 caracteres de largo.'
                    },
                }            
            },            
            'direcciones[]': {
                validators: {
                    notEmpty: {
                        message: 'La direccion no puede estar vacia.'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ -]+$/,
                        message: 'La direccion no puede tener caracteres especiales.'
                    },
                    stringLength: {
                        min: 5,                        
                        message: 'La dirección debe tener más 5 caracteres de largo.'
                    },                                        
                }
            },
            'telefonos[]': {              
                validators: {
                    notEmpty: {
                        message: 'El teléfono no puede estar vacio.'
                    },
                    regexp: {
                        regexp: /^[0-9+()-]+$/,                        
                        message: 'El teléfono no puede tener caracteres especiales ni letras.'
                    },
                }
            },
            'correos[]': {
                validators: {
                    notEmpty: {
                        message: 'El correo no puede estar vacio.'
                    },
                    emailAddress: {
                        message: 'No es un correo válido.'
                    },                                        
                }
            },           
        }
    });
});
$('#abrirmodalContacto').on('hidden.bs.modal', function (){    
    $('#formularioAgregarContacto').bootstrapValidator("resetForm",true);    
});

// $('#abrirmodalEditarContacto').on('show.bs.modal', function (){    
//     $('#formularioEditarContacto').bootstrapValidator({
//         message: 'Este valor no es válido.',
//         feedbackIcons: {
//             // valid: 'glyphicon glyphicon-ok',
//             // invalid: 'glyphicon glyphicon-remove',
//             // validating: 'glyphicon glyphicon-refresh'

//             valid: 'fa fa-check',
//             invalid: 'fa fa-times',
//             validating: 'fa fa-refresh'
//         },
//         // excluded: [':disabled'],
//         fields: {            
//             cont_nombre: {
//                 message: 'El nombre no es valido.',
//                 validators: {
//                     notEmpty: {
//                         message: 'El nombre no puede estar vacio.'
//                     },
//                     stringLength: {
//                         min: 5,
//                         max: 50,
//                         message: 'El nombre debe tener entre 5 y 50 caracteres de largo.'
//                     },
//                 }            
//             },            
//             'direcciones[]': {
//                 validators: {
//                     notEmpty: {
//                         message: 'La direccion no puede estar vacia.'
//                     },
//                     regexp: {
//                         regexp: /^[a-zA-Z0-9_ -]+$/,
//                         message: 'La direccion no puede tener caracteres especiales.'
//                     },
//                     stringLength: {
//                         min: 5,                        
//                         message: 'La dirección debe tener más 5 caracteres de largo.'
//                     },                                        
//                 }
//             },
//             'telefonos[]': {              
//                 validators: {
//                     notEmpty: {
//                         message: 'El teléfono no puede estar vacio.'
//                     },
//                     // regexp: {
//                     //     regexp: /^[0-9+()-]+$/,                        
//                     //     message: 'El teléfono no puede tener caracteres especiales ni letras.'
//                     // },
//                 }
//             },
//             'correos[]': {
//                 validators: {
//                     notEmpty: {
//                         message: 'El correo no puede estar vacio.'
//                     },
//                     emailAddress: {
//                         message: 'No es un correo válido.'
//                     },                                        
//                 }
//             },            
//         }
//     });
// });
// $('#abrirmodalEditarContacto').on('hidden.bs.modal', function (){    
//     $('#formularioEditarContacto').bootstrapValidator("resetForm",true);    
// });
// FIN VALIDACION DE FORMULARIOS