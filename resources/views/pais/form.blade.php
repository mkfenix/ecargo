<div class="form-group row">
    <label class="col-md-3 form-control-label" for="text-input">Codigo</label>
    <div class="col-md-9">
        <input type="text" name="codigo" id="pais_codigo" class="form-control" placeholder="Codigo de Pais"require pattern="[a-zA-Z]{0,30}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="descripcion">Descripción</label>
    <div class="col-md-9">
    <input type="text" name="descripcion" id="pais_descripcion" class="form-control" placeholder="Ingrese Pais">
    </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
  <button type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i> Guardar</button>
                           
</div>