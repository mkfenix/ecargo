@extends('principal')
@section('contenido')
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
	                  	<div class="col-md-12">
		                          <!--Contenido-->
                                  <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <div class="card-header">

                       <h2>Listado de Pais</h2><br/>
                      
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodal">
                            <i class="fa fa-plus fa-2x"></i>&nbsp;&nbsp;Agregar Pais
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-6">
                            {!!Form::open(array('url'=>'pais','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">
                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <span class="input-group-btn">
                                      <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                </div>
                                {{Form::close()}}
                                <div>
                                <table class="table table-bordered table-striped table-sm">
                                    <thead>
                                     <tr class="bg-primary">
                                   
                                        <th>Codigo</th>
                                        <th>Descripcion</th>
                                        <th>Editar</th>
                                     </tr>
                                    </thead>
                             <tbody>
                               @foreach($pais as $pa)
                                <tr>
                                    
                                    <td>{{$pa->pais_codigo}}</td>
                                    <td>{{$pa->pais_descripcion}}</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-md" data-pais_id="{{$pa->pais_id}}" data-codigo="{{$pa->pais_codigo}}" data-descripcion="{{$pa->pais_descripcion}}" data-toggle="modal" data-target="#abrirmodalEditar">

                                          <i class="fa fa-edit fa-2x"></i> Editar
                                        </button> &nbsp;
                                    </td>
                                </tr>
                                
                              @endforeach
                              
                            </tbody>
                        </table>
                        {{$pais->render()}}
                        </div>
                        <div class="modal fade" id="abrirmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Agregar Pais</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                            
                            
                             

                            <form action="{{route('pais.store')}}" method="post" class="form-horizontal">
                               
                                {{csrf_field()}}
                                @include('pais.form')

                            </form>

                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
             <!-- empieza EDITAR PAIS -->
            <div class="modal fade" id="abrirmodalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Actualizar Pais</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                             

                            <form action="{{route('pais.update','test')}}" method="post" class="form-horizontal">
                                
                                {{method_field('patch')}}
                                {{csrf_field()}}

                                <input type="hidden" id="pais_id" name="pais_id" value="">
                                
                                @include('pais.form')

                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--Fin del modal-->

                        
                            </div>
                        </div>
          <!--Fin Contenido-->
                           </div>
                        </div>
		                    
                  		</div>
                  	</div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection