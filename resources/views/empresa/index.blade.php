@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
                        <div class="col-md-12">
		                          <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <!-- VER DATOS EN FORMATO ARRAY <pre>{{var_dump($empresa->toArray())}}</pre>-->
                    <div class="card-header">
                       <h2>Listado de Empresa</h2><br/>                      
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodalEmpresa">
                            <i class="fa fa-plus fa"></i>&nbsp;&nbsp;Agregar Empresa
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
							<!--BUSCADOR INICIO-->
                            {!!Form::open(array('url'=>'empresa','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <span class="input-group-btn">
                                        <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                </div>
                            {{Form::close()}}
							<!--BUSCADOR FIN-->
                                <table class="table table-bordered table-striped table-sm">
                                    <thead>
                                        <tr class="bg-primary">                                   
                                            <th>Nombre</th>
                                            <th>Código</th>
                                            <th>Tipo Documento</th>
                                            <th>Número Documento</th>
                                            <th>Dirección Central</th>
                                            <th>Teléfonos</th>
                                            <th>Logo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($empresa as $emp)
                                    <tr>                                    
                                        <td>{{$emp->emp_nombre}}</td>
                                        <td>{{$emp->emp_codigo}}</td>
                                        <td>{{$emp->emp_tipo_doc}}</td>
                                        <td>{{$emp->emp_num_doc}}</td>
                                        <td>{{$emp->emp_dircentral}}</td>
                                        <td>
                                            <select>
                                                @foreach($emp->emp_telefonos as $telefono)
                                                     <option value="{{$telefono}}" class ="form-control" style="width:110px;">{{$telefono}}</option>
                                                @endforeach
                                            </select>
                                        </td>                                         
                                        <td class="col-md-2">
                                            <img src="{{asset('storage/img/empresa/'.$emp->emp_logo)}}" id="logo1" alt="{{$emp->emp_nombre}}" class="img-responsive" width="100" height="100">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-md" data-emp_id="{{$emp->emp_id}}" data-emp_nombre="{{$emp->emp_nombre}}" data-emp_codigo="{{$emp->emp_codigo}}" data-emp_tipo_doc="{{$emp->emp_tipo_doc}}" data-emp_num_doc="{{$emp->emp_num_doc}}" data-emp_dircentral="{{$emp->emp_dircentral}}" data-emp_telefonos="{{implode(',',$emp->emp_telefonos)}}" data-emp_logo="{{$emp->emp_logo}}" data-toggle="modal" data-target="#abrirmodalEditarEmpresa">
                                            <i class="fa fa-edit fa-2x"></i>Editar
                                            </button>&nbsp;                                        
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-md" data-emp_id="{{$emp->emp_id}}" data-toggle="modal" data-target="#abrirmodalEliminarEmpresa"> 
                                            <i class="fa fa-edit fa-2x"></i>Eliminar
                                            </button> &nbsp;
                                        </td>                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            <!-- PAGINACION INICIO-->                            
                            {{$empresa->appends(['buscarTexto' => $buscarTexto])->links()}}
                            <!-- PAGINACION FIN-->
                            <!-- INICIO DEL MODAL AGREGAR-->
                            <div class="modal fade" id="abrirmodalEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                    			<div class="modal-dialog modal-primary modal-lg" role="document">
                        			<div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Agregar Empresa</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                       
                                        <div class="modal-body">     
                                            <form id="formularioAgregar" action="{{route('empresa.store')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                @include('empresa.form')                        
                                            </form>
                                        </div>                        
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>
                            <!-- INICIO MODAL ACTUALIZAR -->
                            <div class="modal fade" id="abrirmodalEditarEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Actualizar Empresa</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                       
                                <div class="modal-body">
                                    <form id="formularioEditar" action="{{route('empresa.update','test')}}" method="post" class="form-horizontal" enctype="multipart/form-data">  
                                        {{method_field('patch')}}
                                        {{csrf_field()}}
                                        <input type="hidden" id="emp_id" name="emp_id" value="">@include('empresa.form')
                                    </form>
                                </div>                        
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ACTUALIZAR-->
                            <!--INICIO MODAL ELIMINAR-->
                             <div class="modal fade" id="abrirmodalEliminarEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Eliminar Empresa</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                                       
                                        <div class="modal-body">
                                            <form action="{{route('empresa.destroy','test')}}" method="post" class="form-horizontal">
                                                {{method_field('delete')}}
                                                {{csrf_field()}}
                                                <!--CAMPO OCULTO PARA MANEJAR EL ID DE EMPRESA-->
                                                <input type="hidden" id="emp_id" name="emp_id" value="">      
                                                <p>Está seguro que desea Eliminar la Empresa?</p>
                                                <div class="modal-footer">
                                                	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                                	<button type="submit" class="btn btn-success" ><i class = "fa fa-lock fa-2x"></i>Aceptar</button>  	
                                                </div>
                                            </form>
                                        </div>                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ELIMINAR-->                        
                            </div>
                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>		                    
                  		    </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->	
@endsection