<div class="form-group row" >
    <label class="col-md-3 form-control-label" for="emp_nombre">Nombre</label>
    <div class="col-md-8 inputGroupContainer">        
        <input type="text" name="nombre" id="emp_nombre" class="form-control" placeholder="Nombre de Empresa">        
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_codigo">Código</label>
    <div class="col-md-3 inputGroupContainer">
        <input type="text" name="codigo" id="emp_codigo" class="form-control" placeholder="Código Empresa">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_tipo_doc">Tipo Documento</label>
    <div class="col-md-2">        
        <select name="tipo_doc" id="emp_tipo_doc" class="form-control">
            <option value="NIT" selected id="nit">NIT</option>
            <option value="RUC" id="ruc">RUC</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_num_doc">Número Documento</label>
    <div class="col-md-3 inputGroupContainer">
        <input type="text" name="num_doc" id="emp_num_doc" class="form-control" placeholder="Número de Documento">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_dircentral">Dirección</label>
    <div class="col-md-8 inputGroupContainer">
        <input type="text" name="dircentral" id="emp_dircentral" class="form-control" placeholder="Direccion Of. Central">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_logo">Logo Empresa</label>
    <div class="col-md-2 inputGroupContainer">
        <input type="file" name="logo" id="emp_logo">
        <!-- <input type="file" name="logo" id="emp_logo" class="form-control"> -->
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label">Teléfono(s)</label>
    <div class="col-md-4 inputGroupContainer">        
        <table id="tabla" class= "tabla_wrapper">
            <tr>                            
                <td>
                    <label class="form-control-label" style="width:185px;">Número(s)</label>
                </td>                
                <td>
                    <a href="#" class="add_button" title="Añadir Telefono"><button id="adicionar" type="button" class="btn btn-success" onclick="masTelefonosEmpresa()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                    
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div style="display: none;" class="form-group row">
    <label class="col-md-3 form-control-label" for="emp_estado">Estado</label>
    <div class="col-md-9">
        <input type="number" name="estado" id="emp_estado" value="1" class="form-control">
    </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
  <button id="btnguardar" type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i>Guardar</button> 
</div>