<div class="form-group row">
    <label class="col-md-3 form-control-label" for="text-input">Tamaño</label>
    <div class="col-md-9">
        <input type="text" name="tamano" id="cont_tamano" class="form-control" placeholder="Tamaño del Contenedor">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="tipo">Tipo</label>
    <div class="col-md-9">
    <input type="text" name="tipo" id="cont_tipo" class="form-control" placeholder="Ingrese tipo de contenedor">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label" for="descripcion">Descripcion</label>
    <div class="col-md-9">
    <input type="text" name="descripcion" id="cont_descripcion" class="form-control" placeholder="Ingrese descripcion del contenedor">
    </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
  <button type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i> Guardar</button>
                           
</div>