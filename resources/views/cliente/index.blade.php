@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
                        <div class="col-md-12">
		                          <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    
                    <div class="card-header">
                       <h2>Listado de Clientes</h2><br/>                       
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodalCliente" onclick="cargarCiudades(); ocultarCampos();">
                            <i class="fa fa-plus fa"></i>&nbsp;&nbsp;Agregar Cliente
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
							<!--BUSCADOR INICIO-->
                            {!!Form::open(array('url'=>'cliente','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <span class="input-group-btn">
                                        <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                </div>
                            {{Form::close()}}
							<!--BUSCADOR FIN-->                            
                                <table class="table table-bordered table-striped table-sm" id="tabla">
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>Nombre</th>                                     
                                            <th>Tipo Documento</th>
                                            <th>Número Documento</th>
                                            <th>Identificacion Ciudadano</th>
                                            <th>Dirección</th>                                 
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cliente as $cli)
                                    <tr>                                    
                                        <td>{{$cli->cli_nombre}}</td>
                                        <td>{{$cli->cli_tipo_doc}}</td>
                                        <td>{{$cli->cli_num_doc}}</td>
                                        <td>{{$cli->cli_ci}}</td>
                                        <td>
                                            <select>
                                                @foreach($cli->direcciones as $dir)
                                                     <option value="{{$dir->dir_descripcion}}" class ="form-control">{{$dir->dir_ciudad}}:&nbsp;{{$dir->dir_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select>
                                                @foreach($cli->telefonos as $tel)
                                                     <option value="{{$tel->telefono_numero}}" class ="form-control">{{$tel->telefono_tipo}}:&nbsp;{{$tel->telefono_numero}}</option>
                                                @endforeach
                                            </select>
                                        </td>                                        
                                        <td>
                                            <select>
                                                    @foreach($cli->correos()->get() as $cor)
                                                         <option value="{{$cor->correo_descripcion}}" class ="form-control">{{$cor->correo_descripcion}}</option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{url('/cliente/'.$cli->cli_id.'/contacto')}}">Contacto</a>
                                        </td>                    
                                        <td>
                                            <button type="button" class="btn btn-info btn-md" data-cli_id="{{$cli->cli_id}}" data-cli_nombre="{{$cli->cli_nombre}}" data-cli_tipo_doc="{{$cli->cli_tipo_doc}}" data-cli_num_doc="{{$cli->cli_num_doc}}" data-cli_ci="{{$cli->cli_ci}}" data-telefonos="{{json_encode($cli->telefonos()->get())}}"
                                            data-direcciones="{{json_encode($cli->direcciones()->get())}}"
                                            data-correos="{{json_encode($cli->correos()->get())}}"
                                            data-ciudades="{{json_encode($ciudad)}}"
                                            data-cli_tipo="{{$cli->cli_tipo}}"
                                            data-toggle="modal" data-target="#abrirmodalEditarCliente"onclick="cargarCiudades();">
                                            <i class="fa fa-edit fa"></i>Editar
                                            </button>&nbsp;                           
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-md" data-cli_id="{{$cli->cli_id}}" data-toggle="modal" data-target="#abrirmodalEliminarCliente"> 
                                            <i class="fa fa-edit fa"></i>Eliminar
                                            </button> &nbsp;
                                        </td>                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>                                
                            <!-- PAGINACION INICIO-->                            
                            {{$cliente->appends(['buscarTexto' => $buscarTexto])->links()}}
                            <!-- PAGINACION FIN-->

                            <!-- INICIO DEL MODAL AGREGAR-->
                            <div class="modal fade" id="abrirmodalCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                    			<div class="modal-dialog modal-primary modal-lg" role="document">
                        			<div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Agregar Cliente</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                       
                                        <div class="modal-body">
                                            <form id="formularioAgregarCliente" action="{{route('cliente.store')}}" method="post" class="form-horizontal">
                                                {{csrf_field()}}
                                                @include('cliente.form')   
                                            </form>
                                        </div>                        
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>                            
                            <!-- INICIO MODAL ACTUALIZAR -->
                            <div class="modal fade" id="abrirmodalEditarCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Actualizar Cliente</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                      
                                <div class="modal-body">
                                    <form id="formularioEditarCliente" action="{{route('cliente.update','test')}}" method="post" class="form-horizontal">  
                                        {{method_field('patch')}}
                                        {{csrf_field()}}
                                        <input type="hidden" id="cli_id" name="cli_id" value="">
                                        @include('cliente.form')
                                    </form>                                
                                </div>
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ACTUALIZAR-->
                            <!--INICIO MODAL ELIMINAR-->
                            <div class="modal fade" id="abrirmodalEliminarCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Eliminar Cliente</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                                       
                                        <div class="modal-body">
                                            <form action="{{route('cliente.destroy','test')}}" method="post" class="form-horizontal">
                                                {{method_field('delete')}}
                                                {{csrf_field()}}
                                                <!--CAMPO OCULTO PARA MANEJAR EL ID DEL CLIENTE-->
                                                <input type="hidden" id="cli_id" name="cli_id" value="">      
                                                <p>Está seguro que desea Eliminar al Cliente?</p>
                                                <div class="modal-footer">
                                                	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                                	<button type="submit" class="btn btn-success" ><i class = "fa fa-check fa-2x"></i>Aceptar</button>  	
                                                </div>
                                            </form>
                                        </div>                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ELIMINAR-->
                            </div>
                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>		                    
                  		    </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->	
@endsection
