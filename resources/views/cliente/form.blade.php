<div class="form-group row cli_tipo" id="tipoCliente" style="display: none;">
    <label class="col-md-3 form-control-label" for="cli_tipo">Tipo de Cliente a Registrar</label>
    <div class="col-md-3">        
        <select name="cli_tipo" id="cli_tipo" class="form-control" onchange="mostrarCampos()">
            <option value="" selected disabled>--Seleccione--</option>
            <option value="empresa">Empresa</option>
            <option value="persona">Persona Natural</option>
        </select>
    </div>
</div>

<div class="form-group row" id="nombreCliente">    
    <label class="col-md-3 form-control-label" for="cli_nombre">Nombre</label>    
    <div class="col-md-8">        
        <input type="text" name="nombre" id="cli_nombre" class="form-control" placeholder="Nombre del Cliente">
    </div>
</div>

<div class="form-group row carnetCliente" id="carnetCliente">
    <label class="col-md-3 form-control-label" for="cli_ci">Carnet de Identidad</label>
    <div class="col-md-3">
        <input type="text" name="ci" id="cli_ci" class="form-control" placeholder="Carnet de Identidad">
    </div>
</div>

<div class="form-group row" id="tipoDocumentoCliente">
    <label class="col-md-3 form-control-label" for="cli_tipo_doc">Tipo Documento</label>
    <div class="col-md-2">        
        <select name="tipo_doc" id="cli_tipo_doc" class="form-control">
            <option value="" selected disabled="">--Seleccione--</option>            
            <option value="NIT">Nit</option>
            <option value="RUC">Ruc</option>
            <option value="Ninguno">Ninguno</option>
        </select>
    </div>
</div>

<div class="form-group row" id="numeroDocumentoCliente">
    <label class="col-md-3 form-control-label" for="cli_num_doc">Número Documento</label>
    <div class="col-md-3">
        <input type="text" name="num_doc" id="cli_num_doc" class="form-control" placeholder="Número de Documento">
    </div>
</div>

<div class="form-group row" id="direccionCliente">
    <label class="col-md-3 form-control-label">Dirección(es)</label>
    <div class="col-md-9">        
        <table id="tablaDireccion" class= "tabla_wrapperDireccion">
            <tr> 
                <td>
                    <!-- PARA QUE ESTEN ALINEADOS LOS CAMPOS DE SELECT PARA DIRECCION Y CIUDAD -->
                </td>                           
                <td>
                    <label class="form-control-label"></label>
                </td>                
                <td>
                    <a href="#" class="add_buttonDireccion" title="Añadir Dirección"><button id="adicionar" type="button" class="btn btn-success" onclick="masDirecciones()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                   
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div class="form-group row" id="telefonoCliente">
    <label class="col-md-3 form-control-label">Teléfono(s)</label>
    <div class="col-md-4">        
        <table id="tablaTelefono" class= "tabla_wrapperTelefono">
            <tr>
                <td>
                    <!-- PARA QUE ESTEN ALINEADOS LOS CAMPOS DE SELECT PARA TIPO DE TELEFONO Y NUMERO DE TELEFONO -->
                </td>                            
                <td>
                    <label class="form-control-label" style="width:185px;">Número(s)</label>
                </td>                
                <td>
                    <a href="#" class="add_buttonTelefono" title="Add field"><button id="adicionar" type="button" class="btn btn-success" onclick="masTelefonos()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                    
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div class="form-group row" id="correoCliente">
    <label class="col-md-3 form-control-label">Correo(s)</label>
    <div class="col-md-9">        
        <table id="tablaCorreo" class= "tabla_wrapperCorreo">
            <tr>                            
                <td>
                    <label class="form-control-label"></label>
                </td>                
                <td>
                    <a href="#" class="add_buttonCorreo" title="Add field"><button id="adicionar" type="button" class="btn btn-success" onclick="masCorreos()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                    
                </td>
            </tr>            
        </table>        
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
    <button id="btnguardar" type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i>Guardar</button> 
</div>