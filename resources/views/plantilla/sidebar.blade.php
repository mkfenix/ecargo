 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
                    
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header"></li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Empresa</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu"> 

                <li><a class="nav-link" href="{{url('empresa')}}" onclick="event.preventDefault(); document.getElementById('empresa-form').submit();"><i class="fa fa-circle-o"></i>Registro Empresas</a>
                  <form id="empresa-form" action ="{{url('empresa')}}" method="GET" style="display: none;">
                    {{csrf_field()}}                    
                  </form>                    
                </li>

                <li><a class="nav-link" href="{{url('sucursal')}}" onclick="event.preventDefault(); document.getElementById('sucursal-form').submit();"><i class="fa fa-circle-o"></i>Registro Sucursales</a>
                  <form id="sucursal-form" action ="{{url('sucursal')}}" method="GET" style="display: none;">
                    {{csrf_field()}}                    
                  </form>                    
                </li>
                
                <li><a href="empresa/empleado"><i class="fa fa-circle-o"></i> Registro de Empleados</a></li>
                <li><a href="empresa/rol"><i class="fa fa-circle-o"></i> Registro de Roles</a></li>
                <li><a href="empresa/usuario"><i class="fa fa-circle-o"></i> Registro de Usuarios</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i>
                <span>Variables globales</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a  class="nav-link" href="{{url('pais')}}"onclick="event.preventDefault(); document.getElementById('pais-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Pais</a>
                    <form id="pais-form" action="{{url('pais')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                    </form>
                
                </li>
                <li><a  class="nav-link" href="{{url('ciudad')}}"onclick="event.preventDefault(); document.getElementById('ciudad-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Ciudad</a>
                      <form id="ciudad-form" action="{{url('ciudad')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>                
                
                </li>
                <li><a href="variables/puertos"><i class="fa fa-circle-o"></i> Registro Puertos Base</a></li>
                <li><a  class="nav-link" href="{{url('condicion')}}"onclick="event.preventDefault(); document.getElementById('condicion-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Condicion</a>
                      <form id="condicion-form" action="{{url('condicion')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>                 
                </li>
                <li><a  class="nav-link" href="{{url('contenedor')}}"onclick="event.preventDefault(); document.getElementById('contenedor-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Contenedor</a>
                      <form id="contenedor-form" action="{{url('contenedor')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>
                
                </li>
                <li><a  class="nav-link" href="{{url('moneda')}}"onclick="event.preventDefault(); document.getElementById('moneda-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Moneda</a>
                      <form id="moneda-form" action="{{url('moneda')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>               
                </li>
                <li><a href="variables/naviera"><i class="fa fa-circle-o"></i> Registro Naviera</a></li>

                <li><a  class="nav-link" href="{{url('identificacion')}}"onclick="event.preventDefault(); document.getElementById('identificacion-form').submit();"><i class="fa fa-circle-o" ></i> Registro Doc. Identificación</a>
                      <form id="identificacion-form" action="{{url('identificacion')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>               
                </li>
                <li><a href="variables/naviera"><i class="fa fa-circle-o"></i> Registro Naviera</a></li>                
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Pricing</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="pricing/agente"><i class="fa fa-circle-o"></i> Registro de Agentes</a></li>
                <li><a href="pricing/fletes"><i class="fa fa-circle-o"></i> Registro de Fletes</a></li>
                <li><a href="pricing/fletebase"><i class="fa fa-circle-o"></i> Registro de Fletes Puerto Base</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Comercial</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a  class="nav-link" href="{{url('proveedor')}}"onclick="event.preventDefault(); document.getElementById('proveedor-form').submit();"><i class="fa fa-circle-o" ></i> Registro de Proveedores</a>
                      <form id="proveedor-form" action="{{url('proveedor')}}" method="GET" style="display: none;">
                            {{csrf_field()}} 
                      </form>                 
                </li>

                <li><a class="nav-link" href="{{url('cliente')}}" onclick="event.preventDefault(); document.getElementById('cliente-form').submit();"><i class="fa fa-circle-o"></i>Registro Clientes</a>
                  <form id="cliente-form" action ="{{url('cliente')}}" method="GET" style="display: none;">
                    {{csrf_field()}}                    
                  </form>                    
                </li></li>

                <li><a href="comercial/cotbase"><i class="fa fa-circle-o"></i> Cotizacion Puertos Base</a></li>
                <li><a href="comercial/cotpuerto"><i class="fa fa-circle-o"></i> Cotizacion Cualquier Puerto</a></li>
                <li><a href="comercial/formulario"><i class="fa fa-circle-o"></i>Confirmacion de Embarque</a></li>

              </ul>
            </li>
                       
            <li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i> <span>Acceso</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="configuracion/usuario"><i class="fa fa-circle-o"></i> Usuarios</a></li>
                
              </ul>
            </li>
             <li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span>Ayuda</span>
                <small class="label pull-right bg-red">PDF</small>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-info-circle"></i> <span>Acerca De...</span>
                <small class="label pull-right bg-yellow">IT</small>
              </a>
            </li>
                        
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>