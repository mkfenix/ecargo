@extends('principal')
@section('contenido')
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
	                  	<div class="col-md-12">
		                          <!--Contenido-->
                                  <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <div class="card-header">

                       <h2>Listado de Condicion</h2><br/>
                      
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodal">
                            <i class="fa fa-plus fa-2x"></i>&nbsp;&nbsp;Agregar Condicion
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-6">
                            {!!Form::open(array('url'=>'condicion','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">
                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                                {{Form::close()}}
                                <table class="table table-bordered table-striped table-sm">
                                    <thead>
                                     <tr class="bg-primary">
                                   
                                        <th>Codigo</th>
                                        <th>Descripcion</th>
                                        <th>Editar</th>
                                     </tr>
                                    </thead>
                             <tbody>
                               @foreach($condicion as $cond)
                                <tr>
                                    
                                    <td>{{$cond->cond_codigo}}</td>
                                    <td>{{$cond->cond_descripcion}}</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-md" data-cond_id="{{$cond->cond_id}}" data-codigo="{{$cond->cond_codigo}}" data-descripcion="{{$cond->cond_descripcion}}" data-toggle="modal" data-target="#abrirmodalEditarCond">

                                          <i class="fa fa-edit fa-2x"></i> Editar
                                        </button> &nbsp;
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                        <div class="modal fade" id="abrirmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Agregar Condicion</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                            
                            
                             

                            <form action="{{route('condicion.store')}}" method="post" class="form-horizontal">
                               
                                {{csrf_field()}}
                                @include('condicion.form')

                            </form>

                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
             <!-- empieza EDITAR Condicion -->
            <div class="modal fade" id="abrirmodalEditarCond" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Actualizar Condicion</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                             

                            <form action="{{route('condicion.update','test')}}" method="post" class="form-horizontal">
                                
                                {{method_field('patch')}}
                                {{csrf_field()}}

                                <input type="hidden" id="cond_id" name="cond_id" value="">
                                
                                @include('condicion.form')

                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--Fin del modal-->

                        
                            </div>
                        </div>
          <!--Fin Contenido-->
                           </div>
                        </div>
		                    
                  		</div>
                  	</div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection