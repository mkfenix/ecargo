@extends('principal')
@section('contenido')
<div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
	                  	<div class="col-md-12">
		                          <!--Contenido-->
                                  <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <div class="card-header">

                       <h2>Listado de Sucursales</h2><br/>
                      
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodal">
                            <i class="fa fa-plus fa-2x"></i>&nbsp;&nbsp;Agregar Sucursal
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-6">
							<!-- PARA BUSCADOR INICIO-->
                            {!!Form::open(array('url'=>'sucursal','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">
                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <span class="input-group-btn">
                                    <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button></span>
                                </div>
                                {{Form::close()}}
							<!-- PARA BUSCADOR FIN-->
                                <table class="table table-bordered table-striped table-sm">
                                    <thead>
                                     <tr class="bg-primary">
                                   
                                        <th>Empresa</th>
                                        <th>Ciudad</th>
                                        <th>Dirección</th>
                                        <!--<th>Zona</th>
                                        <th>Avenida</th>
                                        <th>Estado</th>
                                        <th>Calle</th>
                                        <th>Número</th>
                                        <th>Edificio</th>
                                        <th>Piso</th>
                                        <th>Número Oficina</th>-->

                                     </tr>
                                    </thead>
                             <tbody>
                               @foreach($sucursal as $suc)
                                <tr>                                    
                                    <td>{{$suc->suc_ciudad}}</td>
                                    <td>{{$suc->empresa}}</td>                  
                                    <td>{{$suc->descripcion}}</td>                               
                                    <td>
                                        <button type="button" class="btn btn-info btn-md" data-suc_id="{{$suc->suc_id}}" data-suc_ciudad="{{$suc->suc_ciudad}}" data-toggle="modal" data-target="#abrirmodalEditar">
                                          <i class="fa fa-edit fa-2x"></i>Editar
                                        </button> &nbsp;
                                    <td>
                                        <button type="button" class="btn btn-danger btn-md" data-emp_id="{{$suc->suc_id}}" data-toggle="modal" data-target="#abrirmodalEliminar"> 
                                          <i class="fa fa-edit fa-2x"></i>Eliminar
                                        </button> &nbsp;
                                    </td>
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                        <!-- PARA PAGINACION INICIO-->                        
                         {{$sucursal->appends(['buscarTexto' => $buscarTexto])->links()}}
                        <!-- PARA PAGINACION FIN-->
                        <!-- INICIO DEL MODAL AGRAGAR-->
                        <div class="modal fade" id="abrirmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                			<div class="modal-dialog modal-primary modal-lg" role="document">
                    			<div class="modal-content">
                        		<div class="modal-header">
                            <h4 class="modal-title">Agregar Sucursal</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">     
                            <form action="{{route('sucursal.store')}}" method="post" class="form-horizontal">                            
                                {{csrf_field()}}
                                @include('sucursal.form')                             

                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
             <!-- INICIO MODAL ACTUALIZAR -->
            <div class="modal fade" id="abrirmodalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Actualizar Sucursal</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                             

                            <form action="{{route('sucursal.update','test')}}" method="post" class="form-horizontal">
                                
                                {{method_field('patch')}}
                                {{csrf_field()}}

                                <input type="hidden" id="suc_id" name="suc_id" value="">
                                
                                @include('sucursal.form')

                            </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--Fin del modal ELIMINAR-->
             <div class="modal fade" id="abrirmodalEliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-primary modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Eliminar Sucursal</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                        </div>
                       
                        <div class="modal-body">
                             

                            <form action="{{route('sucursal.destroy','test')}}" method="post" class="form-horizontal">
                                
                                {{method_field('delete')}}
                                {{csrf_field()}}
                                <!--CAMPO OCULTO PARA MANEJAR EL ID DE EMPRESA-->
                                <input type="hidden" id="suc_id" name="suc_id" value="">      
                                <p>Está seguro que desea Eliminar la Sucursal?</p>
                                <div class="modal-footer">
                                	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                	<button type="submit" class="btn btn-success" ><i class = "fa fa-lock fa-2x"></i>Aceptar</button>
                                	
                                </div>
                                </form>
                        </div>
                        
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--Fin del modal ELIMINAR-->

                        
                            </div>
                        </div>
          <!--Fin Contenido-->
                           </div>
                        </div>
		                    
                  		</div>
                  	</div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->	
@endsection
