<div class="form-group row">
    <label class="col-md-3 form-control-label" for="text-input">Ciudad</label>
    <div class="col-md-9">
        <input type="text" name="ciudad" id="suc_ciudad" class="form-control" placeholder="Ciudad"require pattern="[a-zA-Z]{0,30}">
    </div>
</div>

<!--PARA JALAR DATOS DE EMPRESA-->
<div class="form-group row">
<label class="col-md-3 form-control-label" for="titulo">Empresa</label>
            
    <div class="col-md-9">
           
        <select class="form-control" name="emp_id" id="emp_id" required="">
                                                
        <option value="0" disabled>Seleccione Empresa</option>
        @foreach($empresa as $emp)
        <option value="{{$emp->emp_id}}">{{$emp->emp_nombre}}</option>      
        @endforeach

        </select>
            
    </div>
</div>

@include('direccion.form')

<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
  <button type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i> Guardar</button>
                           
</div>