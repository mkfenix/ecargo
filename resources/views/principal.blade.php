<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Cargo Overseas Group</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    
    <!-- INICIO BOOSTRAPVALIDATOR CABECERA-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <!-- FIN BOOSTRAPVALIDATOR CABECERA-->    
  </head>
  <body class="hold-transition skin-blue sidebar-mini"> 
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>EC</b>V</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>E-Cargo Overseas Group</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <small class="bg-red">Online</small>
                  <span class="hidden-xs">Usuario</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">                    
                    <p>                      
                      <small></small>
                    </p>
                  </li>                  
                  <!-- Menu Footer-->
                  <li class="user-footer">                    
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Cerrar</a>
                    </div>
                  </li>
                </ul>
              </li>              
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @include("plantilla.sidebar")
      <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <!-- Main content -->
      @yield('contenido')   
        
      <!--Fin-Contenido-->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2019 <a href="www.ecargobolivia.com">E-Cargo Overseas Group</a>.</strong> All rights reserved.
      </footer> 
    </div>

    <!-- SCRIPT -->    
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>    
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>

    <!-- INICIO ESTILO Y SCRIP VALIDACION FORMULARIO -->    
    <link rel="stylesheet" href="{{asset('css/estiloFormulario.css')}}">
    <!-- FIN ESTILO Y SCRIP VALIDACION FORMULARIO -->

    <!-- INICIO SCRIPTS PARA GENERAR DIRECCIONES TELEFONOS Y CORREOS DINAMICAMENTE -->
    <script type="text/javascript">
      function addBootstrapValidator(x, $add){
        if(x==1){//X=1 DIRECCIONES
          if($("#abrirmodalCliente").is(":visible")){
            $('#formularioAgregarCliente').bootstrapValidator('addField', $add);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){
            $('#formularioEditarCliente').bootstrapValidator('addField', $add);
          }  
        }
        if(x==2){//X=2 TELEFONOS
          if($("#abrirmodalCliente").is(":visible")){
            $('#formularioAgregarCliente').bootstrapValidator('addField', $add);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){
            $('#formularioEditarCliente').bootstrapValidator('addField', $add);
          }  
        }
        if(x==3){//X=2 CORREOS
          if($("#abrirmodalCliente").is(":visible")){
            $('#formularioAgregarCliente').bootstrapValidator('addField', $add);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){
            $('#formularioEditarCliente').bootstrapValidator('addField', $add);
          }  
        }        
      }
      function removeBootstrapValidator(x, $remove){        
        if(x==1){//X=1 DIRECCIONES
          if($("#abrirmodalCliente").is(":visible")){            
            $('#formularioAgregarCliente').bootstrapValidator('removeField', $remove);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){            
            $('#formularioEditarCliente').bootstrapValidator('removeField', $remove);
          }  
        }
        if(x==2){//X=2 TELEFONOS
          if($("#abrirmodalCliente").is(":visible")){            
            $('#formularioAgregarCliente').bootstrapValidator('removeField', $remove);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){            
            $('#formularioEditarCliente').bootstrapValidator('removeField', $remove);
          }  
        }
        if(x==3){//X=3 CORREORS
          if($("#abrirmodalCliente").is(":visible")){
            $('#formularioAgregarCliente').bootstrapValidator('removeField', $remove);
          }
          if($("#abrirmodalEditarCliente").is(":visible")){
            $('#formularioEditarCliente').bootstrapValidator('removeField', $remove);
          }  
        }        
      }
      function masDirecciones(){        
        //GENERAMOS EL OPTION MEDIANTE LA VARIABLE ciudades Q OBJENEMOS POR AJAX
        var selectCiudades = "";
        var nuevaFilaDireccion ="";
        $.each(ciudades, function(index, value){          
          selectCiudades += '<option value="'+value.ciu_descripcion+'">'+value.ciu_descripcion+'</option>';
        });
        //GENERAMOSEL SELECT DE CIUDADES
        selectCiudades = '<select name="ciudades[]" class="form-control" style="width:130px;">'+selectCiudades+'</select>';
        if(filasDireccion < maxFilasDireccion){
          //COMPLETAMOS LA FILA QUE CONTIENE EL SELECT + IMPUT DIRECCIONES
          nuevaFilaDireccion = '<tr class="filaDireccion"><td>'+selectCiudades+parcialFilaDireccion
          $(wrapperTablaDireccion).append(nuevaFilaDireccion);
          filasDireccion ++;
          var $addDirecciones = wrapperTablaDireccion.find('[name="direcciones[]"]');
          //LLAMAMOS A LA FUNCION addBootstrapValidator PARA IDENTIFICAR A QUE FORMULARIO SE AGREGARA EL NUEVO CAMPO A VALIDAR 
          addBootstrapValidator(1, $addDirecciones);
        }    
    }
    function menosDirecciones(){       
      $(wrapperTablaDireccion).on("click", ".eliminarDireccion", function(e){
        e.preventDefault();
        var $rowDireccionesCliente = $(this).parents('#tablaDireccion');
        var $rowDireccionesContacto = $(this).parents('#direccionContacto');
        $removeDireccionesCliente = $rowDireccionesCliente.find('[name="direcciones[]"]');
        $removeDireccionesContacto = $rowDireccionesContacto.find('[name="direcciones[]"]');
        removeBootstrapValidator(1, $removeDireccionesCliente);
        $(this).parents('tr').remove();
      });
      filasDireccion --;                    
    }
    function masTelefonos(){      
      if(filasTelefono < maxFilasTelefono){          
        $(wrapperTablaTelefono).append(nuevaFilaTelefono);
        filasTelefono ++;          
        var $addTelefonos = wrapperTablaTelefono.find('[name="telefonos[]"]');
        addBootstrapValidator(2, $addTelefonos);
      }
    }
    function menosTelefonos(){       
      $(wrapperTablaTelefono).on("click", ".eliminarTelefono", function(e){
        e.preventDefault();
        var $rowTelefonos = $(this).parents('#telefonoCliente');
        var $rowTelefonos = $(this).parents('#telefonoContacto');
        $removeTelefonos = $rowTelefonos.find('[name="telefonos[]"]');
        removeBootstrapValidator(2, $removeTelefonos);
        $(this).parents('tr').remove();
      });
      filasTelefono --;
    }
    function masCorreos(){      
      if(filasCorreo < maxFilasCorreo){          
        $(wrapperTablaCorreo).append(nuevaFilaCorreo);
        filasCorreo ++;
        var $addCorreos = wrapperTablaCorreo.find('[name="correos[]"]');    
        addBootstrapValidator(3, $addCorreos);
      }
    }
    function menosCorreos(){       
      $(wrapperTablaCorreo).on("click", ".eliminarCorreo", function(e){
        e.preventDefault();
        var $rowCorreos = $(this).parents('#correoCliente');
        var $rowCorreos = $(this).parents('#correosContacto');
        $removeCorreos = $rowCorreos.find('[name="correos[]"]');
        removeBootstrapValidator(3, $removeCorreos);
        $(this).parents('tr').remove();
      });
      filasCorreo --;                      
    }
    </script>
    <!-- FIN SCRIPTS PARA GENERAR DIRECCIONES TELEFONOS Y CORREOS DINAMICAMENTE -->

    <script type="text/javascript">      
      function cargarModal(){
        var idCliente = $("#btnIdCliente").val();
        $.ajax({
            url: "{{ url('/api/url-ajaxContacto') }}",
            method: 'post',            
            data: {
              //aca asignamos los id de los paises cuyas ciudades queremos q se muestren
               id_cliente:idCliente
            },
            success: function(result){              
              contactos = result.contactos;
              if(contactos == 0){
                $("#abrirmodalContacto").modal("hide");
                $("#abrirmodalClienteContacto").modal("show");  
              }                            
            }            
        });        
      }       
    </script>

    <!-- INICIO SCRIPT EMPRESA -->    
    <script src="{{asset('js/scriptEmpresa.js')}}"></script>
    <!-- FIN SCRIPT EMPRESA -->

    <!-- INICIO SCRIPT CLIENTE -->      
    <script src="{{asset('js/scriptCliente.js')}}"></script>    
    <!-- FIN SCRIPT CLIENTE -->

    <!-- INICIO SCRIPT CONTACTO -->      
    <script src="{{asset('js/scriptContacto.js')}}"></script>    
    <!-- FIN SCRIPT CONTACTO -->

    <!-- INICIO SCRIPT PROVEEDOR -->      
    <script src="{{asset('js/scriptProveedor.js')}}"></script>    
    <!-- FIN SCRIPT PROVEEDOR -->

    <!-- INICIO SRIPT PARA CARGAR LAS CIUDADES CON AJAX USADO PAR AADICIONAR FILAS DE DIRECCIONES -->
    <script type="text/javascript">
    function cargarCiudades(){        
      $.ajax({
          url: "{{ url('/api/url-ajaxCuidad') }}",
          method: 'post',            
          data: {
            //aca asignamos los id de los paises cuyas ciudades queremos q se muestren
             id_bolivia:1,
             id_chile:3
          },
          success: function(result){
             ciudades = result.ciudades;               
          }
      });        
    }
    </script>
    <!-- FIN SRIPT PARA CARGAR LAS CIUDADES CON AJAX USADO PAR AADICIONAR FILAS DE DIRECCIONES -->

    <script>
    /*EDITAR PAIS*/
      $('#abrirmodalEditar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var codigo_modal_editar = button.data('codigo')
        var descripcion_modal_editar = button.data('descripcion')
        var pais_id = button.data('pais_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #pais_codigo').val(codigo_modal_editar);
        modal.find('.modal-body #pais_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #pais_id').val(pais_id);
      })
    </script>

    <script>
      /*EDITAR CONDICION*/
      $('#abrirmodalEditarCond').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var codigo_modal_editar = button.data('codigo')
        var descripcion_modal_editar = button.data('descripcion')
        var cond_id = button.data('cond_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #cond_codigo').val(codigo_modal_editar);
        modal.find('.modal-body #cond_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #cond_id').val(cond_id);
      })
    </script>

    <script>
      /*EDITAR MONEDA*/
      $('#abrirmodalEditarMon').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var codigo_modal_editar = button.data('codigo')
        var descripcion_modal_editar = button.data('descripcion')
        var mon_id = button.data('mon_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #mon_codigo').val(codigo_modal_editar);
        modal.find('.modal-body #mon_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #mon_id').val(mon_id);
      })
    </script>

    <script>
      /*EDITAR CONTENEDOR*/
      $('#abrirmodalEditarCont').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var tamano_modal_editar = button.data('tamano')
        var tipo_modal_editar = button.data('tipo')
        var descripcion_modal_editar = button.data('descripcion')
        var cont_id = button.data('cont_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #cont_tamano').val(tamano_modal_editar);
        modal.find('.modal-body #cont_tipo').val(tipo_modal_editar);
        modal.find('.modal-body #cont_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #cont_id').val(cont_id);
      })
    </script>

    <script>
      /*EDITAR DESTINO*/
      $('#abrirmodalEditarDest').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var descripcion_modal_editar = button.data('descripcion')
        var dest_id = button.data('dest_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #dest_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #dest_id').val(dest_id);
      })
    </script>

    <script>
      /*EDITARION*/
      $('#abrirmodalEditarEmi').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var descripcion_modal_editar = button.data('descripcion')
        var emi_id = button.data('emi_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #emi_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #emi_id').val(emi_id);
      })
    </script>

    <script>
      /*EDITARad*/
      $('#abrirmodalEditarCiu').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var codigo_modal_editar = button.data('codigo')
        var descripcion_modal_editar = button.data('descripcion')
        var pais_modal_editar = button.data('pais')
        var ciu_id = button.data('ciu_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #ciu_codigo').val(codigo_modal_editar);
        modal.find('.modal-body #ciu_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #pais_id').val(pais_modal_editar);
        modal.find('.modal-body #ciu_id').val(ciu_id);
      })  
    </script>

    <script>
    /*EDITAR IDENTIFICACION*/
      $('#abrirmodalEditarIden').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var codigo_modal_editar = button.data('codigo')
        var descripcion_modal_editar = button.data('descripcion')
        var iden_id = button.data('iden_id')
        var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body #iden_codigo').val(codigo_modal_editar);
        modal.find('.modal-body #iden_descripcion').val(descripcion_modal_editar);
        modal.find('.modal-body #iden_id').val(iden_id);
      })
    </script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
    <!-- <script src="{{asset('js/validacionFormularioCliente.js')}}"></script> -->

  </body>
</html>
