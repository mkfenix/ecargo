<div class="form-group row">
    <label class="col-md-3 form-control-label">Codigo</label>
    <div class="col-md-9">
        <input type="text" name="codigo" id="iden_codigo" class="form-control" placeholder="Codigo Doc. Identificación">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 form-control-label">Descripción</label>
    <div class="col-md-9">
    <input type="text" name="descripcion" id="iden_descripcion" class="form-control" placeholder="Descripción del Doc. Identificación">
    </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
  <button type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i> Guardar</button>
                           
</div>