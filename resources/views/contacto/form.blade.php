<div class="form-group row" id="nombreContacto">    
    <label class="col-md-3 form-control-label" for="cont_nombre">Nombre</label>    
    <div class="col-md-8 inputGroupContainer">        
        <input type="text" name="cont_nombre" id="cont_nombre" class="form-control" placeholder="Nombre del Contacto">        
    </div>
</div>

<div class="form-group row" id="estadoCliente">
    <label class="col-md-3 form-control-label">Estado del Contacto</label>
    <div class="col-md-2">        
        <select name="cont_estado" id="cont_estado" class="form-control">            
            <option value="1" selected id="activo">Activo</option>
            <option value="0" id="inactivo">Inactivo</option>
        </select>
    </div>
</div>

<div class="form-group row" id="cli_id" style="display: none;">    
    <div class="col-md-2">        
        <input type="text" name="cli_id" id="cli_id" value="{{$idCliente}}">   
    </div>
</div>

<div class="form-group row" id="direccionContacto">
    <label class="col-md-3 form-control-label">Dirección(es)</label>
    <div class="col-md-9">        
        <table id="tablaDireccion" class= "tabla_wrapperDireccion">
            <tr> 
                <td>
                    <!-- PARA QUE ESTEN ALINEADOS LOS CAMPOS DE SELECT PARA DIRECCION Y CIUDAD -->
                </td>                           
                <td>
                    <label class="form-control-label"></label>
                </td>                
                <td>
                    <a href="#" class="add_buttonDireccion" title="Añadir Dirección"><button id="adicionar" type="button" class="btn btn-success" onclick="masDirecciones()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                   
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div class="form-group row" id="telefonoContacto">
    <label class="col-md-3 form-control-label">Teléfono(s)</label>
    <div class="col-md-4">        
        <table id="tablaTelefono" class= "tabla_wrapperTelefono">
            <tr>
                <td>
                    <!-- PARA QUE ESTEN ALINEADOS LOS CAMPOS DE SELECT PARA TIPO DE TELEFONO Y NUMERO DE TELEFONO -->
                </td>                            
                <td>
                    <label class="form-control-label" style="width:185px;">Número(s)</label>
                </td>                
                <td>
                    <a href="#" class="add_buttonTelefono" title="Add field"><button id="adicionar" type="button" class="btn btn-success" onclick="masTelefonos()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                    
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div class="form-group row" id="correoContacto">
    <label class="col-md-3 form-control-label">Correo(s)</label>
    <div class="col-md-9">        
        <table id="tablaCorreo" class= "tabla_wrapperCorreo">
            <tr>                            
                <td>
                    <label class="form-control-label"></label>
                </td>                
                <td>
                    <a href="#" class="add_buttonCorreo" title="Add field"><button id="adicionar" type="button" class="btn btn-success" onclick="masCorreos()" style="margin-left: 15px; width: 70px;">Mas +</button></a>                    
                </td>
            </tr>            
        </table>        
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-2x"></i> Cerrar</button>
    <button id="btnguardar" type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i>Guardar</button> 
</div>