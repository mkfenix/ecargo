@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
                        <div class="col-md-12">
		                          <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    
                    <div class="card-header">
                        <h2>Listado de Contacto Para:</h2>
                        <h2>{{$cliente->cli_nombre}}</h2><br/>
                        <button class="btn btn-success  btn-lg" type="button" onclick="javascript:window.history.back();" autofocus >
                            <i class="fa fa-reply"></i>&nbsp;&nbsp;Volver
                        </button>
                        <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#abrirmodalContacto" onclick="cargarCiudades();ocultarCamposContacto(); cargarModal();" value="{{$idCliente}}" id="btnIdCliente">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar Contacto
                        </button>                        
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
							<!--BUSCADOR INICIO-->
                            <!-- INICIO MENSAJE NO ENCONTRO BUSQUEDA -->
                            @if ($contacto->isEmpty())
                                <h4 style="color: #FF0000" id="mensajeBusqueda">No se encontro Contacto: {{$buscarTexto}}</h4><br/>                                
                            @endif                        
                            <!-- FIN MENSAJE NO ENCONTRO BUSQUEDA -->
                            {!!Form::open(array('url'=>'contacto','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <!-- INICIO PRUEBA -->
                                    <input type="text" name="idCliente"class="form-control" value="{{$idCliente}}" style="display: none;">
                                    <!-- FIN PRUEBA -->
                                    <span class="input-group-btn">
                                        <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i>Buscar</button>
                                    </span>
                                </div>
                            {{Form::close()}}
							<!--BUSCADOR FIN-->                            
                                <table class="table table-bordered table-striped table-sm" id="tabla">
                                    <thead>
                                        <tr class="bg-primary">                                   
                                            <th>Nombre</th>                      
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contacto as $cont)
                                    <tr>                                    
                                        <td width="300">{{$cont->cont_nombre}}</td>
                                        <td >
                                            <select>
                                                @foreach($cont->direcciones as $dir)
                                                     <option value="{{$dir->dir_descripcion}}" class ="form-control">{{$dir->dir_ciudad}}:&nbsp;{{$dir->dir_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select>
                                                @foreach($cont->telefonos as $tel)
                                                     <option value="{{$tel->telefono_numero}}" class ="form-control">{{$tel->telefono_tipo}}:&nbsp;{{$tel->telefono_numero}}</option>
                                                @endforeach
                                            </select>
                                        </td>                                        
                                        <td>
                                            <select>
                                                    @foreach($cont->correos()->get() as $cor)
                                                         <option value="{{$cor->correo_descripcion}}" class ="form-control">{{$cor->correo_descripcion}}</option>
                                                    @endforeach
                                            </select>
                                        <td>
                                            @if($cont->cont_estado == 1)
                                                ACTIVO
                                            @else
                                                INACTIVO
                                            @endif                                            
                                        </td>
                                        </td>                   
                                        <td>
                                            <button type="button" class="btn btn-info btn-md" data-cont_id="{{$cont->cont_id}}" data-cli_id="{{$idCliente}}" data-cont_nombre="{{$cont->cont_nombre}}" data-cont_estado="{{$cont->cont_estado}}" data-telefonos="{{json_encode($cont->telefonos()->get())}}"
                                            data-direcciones="{{json_encode($cont->direcciones()->get())}}"
                                            data-correos="{{json_encode($cont->correos()->get())}}"
                                            data-ciudades="{{json_encode($ciudad)}}"
                                            data-toggle="modal" data-target="#abrirmodalEditarContacto" onclick="cargarCiudades(); mostrarCamposContacto();">
                                            <i class="fa fa-edit fa"></i>Editar
                                            </button>&nbsp;                           
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-md" data-cont_id="{{$cont->cont_id}}" data-cli_id="{{$idCliente}}" data-toggle="modal" data-target="#abrirmodalEliminarContacto"> 
                                            <i class="fa fa-edit fa"></i>Eliminar
                                            </button> &nbsp;
                                        </td>                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>                                
                            <!-- PAGINACION INICIO-->                            
                            {{$contacto->appends(['buscarTexto' => $buscarTexto])->links()}}
                            <!-- PAGINACION FIN-->
                            <!-- INICIO DEL MODAL AGREGAR-->
                            <div class="modal fade" id="abrirmodalContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                    			<div class="modal-dialog modal-primary modal-lg" role="document">
                        			<div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Agregar Contacto</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                       
                                        <div class="modal-body">     
                                            <form id="formularioAgregarContacto" action="{{route('contacto.store')}}" method="post" class="form-horizontal">
                                                {{csrf_field()}}
                                                @include('contacto.form')      
                                            </form>
                                        </div>                        
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>
                            <!-- INICIO MODAL ACTUALIZAR -->
                            <div class="modal fade" id="abrirmodalEditarContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Actualizar Contacto</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                       
                                <div class="modal-body">
                                    <form id="formularioEditarContacto" action="{{route('contacto.update','test')}}" method="post" class="form-horizontal" enctype="multipart/form-data">  
                                        {{method_field('patch')}}
                                        {{csrf_field()}}
                                        <input type="hidden" id="cont_id" name="cont_id" value="">@include('contacto.form')
                                    </form>
                                </div>                        
                                    </div>
                                <!-- /.modal-content -->
                                </div>
                            <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ACTUALIZAR-->
                            <!--INICIO MODAL ELIMINAR-->
                            <div class="modal fade" id="abrirmodalEliminarContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Eliminar Contacto</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                                       
                                        <div class="modal-body">
                                            <form action="{{route('contacto.destroy','test')}}" method="post" class="form-horizontal">
                                                {{method_field('delete')}}
                                                {{csrf_field()}}
                                                <!--CAMPO OCULTO PARA MANEJAR EL ID DEL CLIENTE-->
                                                <input type="hidden" id="cont_id" name="cont_id" value="">
                                                <input type="hidden" id="cli_id" name="cli_id" value="">      
                                                <p>Está seguro que desea Eliminar este Contacto?</p>
                                                <div class="modal-footer">
                                                	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                                	<button type="submit" class="btn btn-success" ><i class = "fa fa-check fa-2x"></i>Aceptar</button>  	
                                                </div>
                                            </form>
                                        </div>                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ELIMINAR-->

                            <!--INICIO MODAL AÑADIR CONTACTO-->
                            <div class="modal fade" id="abrirmodalClienteContacto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Agregar Contacto</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                                       
                                        <div class="modal-body">
                                            <p>El Contacto que desa registrar es el mismo Cliente? {{$cliente->cli_nombre}}?</p>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                                <a class="btn btn-success" href="{{url('/contacto/'.$idCliente.'/contacto')}}"><i class = "fa fa-check fa-2x"></i>Aceptar</a>   
                                            </div>
                                        </div>                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL AÑADIR CONTACTO-->



                            </div>
                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>		                    
                  		    </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->	
@endsection
