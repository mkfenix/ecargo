@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  	<div class="row">
                        <div class="col-md-12">
		                          <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">

                    <div class="card-header col-md-12">
                       <h2>Listado de Proveedores</h2><br/>
                       <div class="form-group">
                            <div class="col-md-4">
                                <a href="{{ route('proveedor.create') }}">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="cargarIdentificacion();">
                                        <i class="fa fa-plus fa"></i>&nbsp;&nbsp;Agregar Proveedor
                                    </button>
                                </a>
                            </div>
                            @if(Session::has('report'))
                            <div class="col-md-8">
                                <div class="col-md-8">                                        
                                    <div class="caja">{{ Session::get('report') }}</div>
                                </div>
                            </div>
                            @endif
                    </div>                    

                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
							<!--BUSCADOR INICIO-->
                            {!!Form::open(array('url'=>'proveedor','method'=>'GET','autocomplete'=>'on','role'=>'search'))!!} 
                                <div class="input-group">                        
                                    <input type="text" name="buscarTexto"class="form-control" placeholder="Buscar texto" value="{{$buscarTexto}}">
                                    <span class="input-group-btn">
                                        <button type="submit"  class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
                                    </span>
                                </div>
                            {{Form::close()}}
							<!--BUSCADOR FIN-->                            
                                <table class="table table-bordered table-striped table-sm" id="tabla">
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>Nombre</th>                                     
                                            <th>Tipo Documento</th>
                                            <th>Número Documento</th>
                                            <th>Dirección</th>                                 
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($proveedor as $prov)
                                    <tr>                                    
                                        <td>{{$prov->prov_nombre}}</td>
                                        <td>{{$prov->prov_tipoDoc}}</td>
                                        <td>{{$prov->prov_numDoc}}</td>
                                        <td>
                                            <select>
                                                @foreach($prov->direcciones as $dir)
                                                     <option value="{{$dir->dir_descripcion}}" class ="form-control">{{$dir->dir_ciudad}}:&nbsp;{{$dir->dir_descripcion}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select>
                                                @foreach($prov->telefonos as $tel)
                                                     <option value="{{$tel->telefono_numero}}" class ="form-control">{{$tel->telefono_tipo}}:&nbsp;{{$tel->telefono_numero}}</option>
                                                @endforeach
                                            </select>
                                        </td>                                        
                                        <td>
                                            <select>
                                                    @foreach($prov->correos()->get() as $cor)
                                                         <option value="{{$cor->correo_descripcion}}" class ="form-control">{{$cor->correo_descripcion}}</option>
                                                    @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{url('/proveedor/'.$prov->prov_id.'/contacto')}}">Contacto</a>
                                        </td>                    
                                        <td>
                                            <a href="{{ route('proveedor.edit' ,['$id' => $prov->prov_id]) }}">
                                                <button type="button" class="btn btn-info btn-md" onclick="cargarCiudades();">
                                                <i class="fa fa-edit fa"></i>Editar
                                                </button>&nbsp;
                                            </a>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-md" data-prov_id="{{$prov->prov_id}}"data-toggle="modal" data-target="#abrirmodalEliminarProveedor"> 
                                            <i class="fa fa-edit fa"></i>Eliminar
                                            </button> &nbsp;
                                        </td>                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>                                
                            <!-- PAGINACION INICIO-->                            
                            {{$proveedor->appends(['buscarTexto' => $buscarTexto])->links()}}
                            <!-- PAGINACION FIN-->                          
                             
                            <!--INICIO MODAL ELIMINAR-->
                            <div class="modal fade" id="abrirmodalEliminarProveedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-primary modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Eliminar Proveedor</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>                                       
                                        <div class="modal-body">
                                            <form action="{{route('proveedor.destroy','test')}}" method="post" class="form-horizontal">
                                                {{method_field('delete')}}
                                                {{csrf_field()}}
                                                <!--CAMPO OCULTO PARA MANEJAR EL ID DEL CLIENTE-->
                                                <input type="hidden" id="prov_id" name="prov_id" value="">      
                                                <p>Está seguro que desea Eliminar al Proveedor?</p>
                                                <div class="modal-footer">
                                                	<button type="button" class="btn btn-danger" data-dismiss="modal"><i class = "fa fa-times fa-2x"></i>Cerrar</button>
                                                	<button type="submit" class="btn btn-success" ><i class = "fa fa-check fa-2x"></i>Aceptar</button>  	
                                                </div>
                                            </form>
                                        </div>                                        
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--FIN MODAL ELIMINAR-->

                            </div>
                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>		                    
                  		    </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->	
@endsection
