@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                                  <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <!-- BOTON PARA VOLVER -->
                    <div class="card-header col-md-12">                        
                        <h2>Registro de Proveedores</h2><br/>
                        <div class="form-group">
                            <div class="col-md-2">                                    
                                <button class="btn btn-primary btn-lg" type="button" href="{{url('proveedor')}}" onclick="event.preventDefault(); document.getElementById('proveedor-form').submit();">                   
                                    <i class="fa fa-reply" aria-hidden="true"></i></i>&nbsp;&nbsp;Volver</a>
                                    <form id="cliente-form" action="{{url('proveedor')}}" method="GET"style="display: none;">
                                            {{csrf_field()}} 
                                    </form>
                                </button>
                            </div>
                            <!-- INICIO NOTIFICACION AL REGISTRAR UN PROVEEDOR -->
                            @if(Session::has('report'))
                            <div class="col-md-8">
                                <div class="col-md-8">                                        
                                    <div class="caja">{{ Session::get('report') }}</div>
                                </div>
                            </div>
                            @endif
                            <!-- FIN NOTIFICACION AL REGISTRAR UN PROVEEDOR -->
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="form-horizontal">
                            <div class="col-md-12">

                                <!-- INICIO FORMULARIO-->
                                <form id="proveedorForm" method="POST" class="form-horizontal" action="{{ route('proveedor.store') }}">
                                    {{ csrf_field() }}

                                    <!-- INICIO NOMBRE -->
                                    <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}" id="nombreProveedor">    
                                        <label class="col-md-3 control-label">Nombre</label>
                                        <div class="col-md-8">        
                                            <input type="text" name="nombre" id="prov_nombre"
                                            class="form-control" placeholder="Nombre del Proveedor" value="{{ old('nombre') }}" autofocus>
                                            @if ($errors->has('nombre'))
                                                <small class="form-text text-danger">{{ $errors->first('nombre') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- FIN NOMBRE -->

                                    <!-- INICIO IDENTIFICACION -->
                                    <div class="form-group">
                                        <label class="col-md-3 form-control-label">Tipo Documento</label>
                                        <div class="col-md-2">
                                            <select name="prov_tipoDoc" class="form-control">
                                                @foreach ($identificacion as $iden)
                                                    <option value="{{$iden->iden_codigo}}">
                                                        {{$iden->iden_codigo}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- FIN IDENTIFICACION -->

                                    <!-- INICIO NUMERO DE IDENTIFICACION -->
                                    <div class="form-group {{ $errors->has('prov_numDoc') ? ' has-error' : '' }}" id="nombreProveedor">    
                                        <label class="col-md-3 control-label">Número de Documento</label>
                                        <div class="col-md-4">        
                                            <input type="text" name="prov_numDoc" id="prov_numDoc"
                                            class="form-control" placeholder="Número de Documento del Proveedor" value="{{ old('prov_numDoc') }}" autofocus>
                                            @if ($errors->has('prov_numDoc'))
                                                <small class="form-text text-danger">{{ $errors->first('prov_numDoc') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- FIN NUMERO DE IDENTIFICACION -->

                                    <!-- INICIO DIRECCION-->                                    
                                    @if (count($direcciones) > 0)
                                    <!-- INICIO MUESTRA CUANDO SI HAY ERRORES -->
                                        @foreach ($direcciones as $indice => $direccion)
                                            <div class="form-group {{ $errors->has('direcciones.'.$indice)? ' has-error' : '' }}">
                                                <label class="col-lg-3 control-label">Dirección</label>
                                                <div class="col-md-2">
                                                    <select name="ciudades[]" class="form-control">
                                                        @foreach ($ciudad as $ciu)
                                                            <option value="{{$ciu->ciu_descripcion}}">
                                                                {{$ciu->ciu_descripcion}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <input class="form-control" type="text" name="direcciones[]" placeholder="Dirección #{{$indice+1}}" value="{{ old('direcciones.'.$indice ) }}" autofocus/>
                                                    @if ($errors->has('direcciones.'.$indice))
                                                        <small class="form-text text-danger">{{ $errors->first('direcciones.'.$indice) }}</small>
                                                    @endif
                                                </div>                                            
                                            </div>                                        
                                        @endforeach
                                    <!-- FIN MUESTRA CUANDO SI HAY ERRORES -->
                                    @else
                                        <!-- INICIO MUESTRA CUANDO NO HAY ERRORES -->
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Dirección</label>
                                            <div class="col-md-2">
                                                <select name="ciudades[]" class="form-control">
                                                    @foreach ($ciudad as $ciu)
                                                        <option value="{{$ciu->ciu_descripcion}}">
                                                            {{$ciu->ciu_descripcion}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control" type="text" name="direcciones[]" placeholder="Dirección #1"  autofocus/>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-success addButton" data-template="direcciones">&nbsp; Mas +</button>
                                            </div>
                                        </div>

                                        <!-- INICIO CLON -->
                                        <div class="form-group hide" id="direccionesTemplate">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-md-2">
                                                <select name="ciudades[]" class="form-control">
                                                    @foreach ($ciudad as $ciu)
                                                        <option value="{{$ciu->ciu_descripcion}}">
                                                            {{$ciu->ciu_descripcion}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control" type="text"/>
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-warning removeButton" id="btn_quitarDireccion" data-template="direcciones">Quitar -</button>
                                            </div>
                                        </div>
                                        <!-- FIN CLON -->
                                        <!-- FIN MUESTRA CUANDO NO HAY ERRORES -->
                                    @endif
                                    <!-- FIN DIRECCION-->

                                    <!-- INICIO TELEFONO-->
                                    @if (count($telefonos) > 0)
                                        <!-- INICIO MUESTRA CUANDO SI HAY ERRORES -->
                                        @foreach ($telefonos as $indice => $telefono)
                                            <div class="form-group {{ $errors->has('telefonos.'.$indice)? ' has-error' : '' }}">
                                                <label class="col-lg-3 control-label">Teléfono</label>
                                                <div class="col-md-2">
                                                    <select name="telefono_tipo[]" class="form-control">
                                                        <option value="Celular" selected id="celular">Celular</option>
                                                        <option value="Fijo" id="fijo">Fijo</option>
                                                        <option value="Fax" id="fax">Fax</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-4">
                                                    <input class="form-control" type="text" name="telefonos[]" placeholder="Teléfono #{{$indice+1}}" value="{{ old('telefonos.'.$indice ) }}" autofocus/>
                                                    @if ($errors->has('telefonos.'.$indice))
                                                        <small class="form-text text-danger">{{ $errors->first('telefonos.'.$indice) }}</small>
                                                    @endif
                                                </div>                                            
                                            </div>                                        
                                        @endforeach
                                        <!-- FIN MUESTRA CUANDO SI HAY ERRORES -->
                                    @else
                                        <!-- INICIO MUESTRA CUANDO NO HAY ERRORES -->
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Teléfono</label>
                                            <div class="col-md-2">
                                                <select name="telefono_tipo[]" class="form-control">
                                                    <option value="Celular" selected id="celular">Celular</option>
                                                    <option value="Fijo" id="fijo">Fijo</option>
                                                    <option value="Fax" id="fax">Fax</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control" type="text" name="telefonos[]" placeholder="Teléfono #1"  autofocus/>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn btn-success addButton" data-template="telefonos">&nbsp; Mas +</button>
                                            </div>
                                        </div>

                                        <!-- INICIO CLON -->
                                        <div class="form-group hide" id="telefonosTemplate">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-md-2">
                                                <select name="telefono_tipo[]" class="form-control">
                                                    <option value="Celular" selected id="celular">Celular</option>
                                                    <option value="Fijo" id="fijo">Fijo</option>
                                                    <option value="Fax" id="fax">Fax</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input class="form-control" type="text"/>
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-warning removeButton" id="btn_quitarTelefono" data-template="telefonos">Quitar -</button>
                                            </div>
                                        </div>
                                        <!-- FIN CLON -->
                                        <!-- FIN MUESTRA CUANDO NO HAY ERRORES -->
                                    @endif
                                    <!-- FIN TELEFONO-->

                                    <!-- INICIO CORREO-->
                                    @if (count($correos) > 0)
                                        @foreach ($correos as $indice => $correo)
                                        <div class="form-group {{ $errors->has('correos.'.$indice)? ' has-error' : '' }}">
                                            <label class="col-lg-3 control-label">Correo</label>
                                            <div class="col-md-6">
                                                <input class="form-control" type="text" name="correos[]" placeholder="Correo #{{$indice+1}}" value="{{ old('correos.'.$indice ) }}" autofocus/>
                                                @if ($errors->has('correos.'.$indice))
                                                    <small class="form-text text-danger">{{ $errors->first('correos.'.$indice) }}</small>
                                                @endif
                                            </div>                                            
                                        </div>                                        
                                        @endforeach
                                    @else
                                        <div class="form-group {{ $errors->has('correos.*') ? ' has-error' : '' }}">
                                            <label class="col-lg-3 control-label">Correo</label>
                                            <div class="col-md-6">
                                                <input class="form-control" type="text" name="correos[]" placeholder="Correo #1" autofocus/>
                                                @if ($errors->has('correos.*'))
                                                    <small class="form-text text-danger">{{ $errors->first('correos.*') }}</small>
                                                @endif
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-success addButton" data-template="correos">&nbsp; Mas +</button>
                                            </div>
                                        </div>

                                        <div class="form-group hide" id="correosTemplate">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-md-6">
                                                <input class="form-control" type="text" />
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="button" class="btn btn-warning removeButton" id="btn_quitarCorreo" data-template="correos">Quitar -</button>
                                            </div>
                                        </div>
                                    @endif
                                    <!-- FIN CORREO-->

                                    <!-- INICIO BOTONES GUARDAR O VOLVER -->
                                    <div class="modal-footer">
                                        <a href="{{ route('proveedor.create') }}">
                                            <button type="button" class="btn btn-warning">
                                                <i class="fa fa-exclamation fa-2x"></i>&nbsp;&nbsp;Limpiar
                                            </button>
                                        </a>
                                        <button id="btn_guardar" type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i>&nbsp;Guardar</button> 
                                    </div>
                                    <!-- FIN BOTONES GUARDAR O VOLVER -->
                                </form>                                
                                <!-- FIN FORMULARIO-->
                            </div>                           

                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>                          
                            </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection