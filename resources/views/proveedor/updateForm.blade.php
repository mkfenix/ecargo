@extends('principal')
@section('contenido')
<div class="content-wrapper">        
        <!-- Main content -->
    <section class="content">          
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">E-Cargo Overseas Group</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>                    
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                                  <!--Contenido-->
                            <div class="container-fluid">
                <!-- Ejemplo de tabla Listado -->
                <div class="card">
                    <!-- BOTON PARA VOLVER -->
                    <div class="card-header col-md-12">                        
                        <h2>Actualizar Proveedor</h2><br/>
                        <div class="form-group">
                            <div class="col-md-2">                                    
                                <button class="btn btn-primary btn-lg" type="button" href="{{url('proveedor')}}" onclick="event.preventDefault(); document.getElementById('proveedor-form').submit();">                   
                                    <i class="fa fa-reply" aria-hidden="true"></i></i>&nbsp;&nbsp;Volver</a>
                                    <form id="cliente-form" action="{{url('proveedor')}}" method="GET"style="display: none;">
                                            {{csrf_field()}} 
                                    </form>
                                </button>
                            </div>
                            <!-- INICIO NOTIFICACION AL REGISTRAR UN PROVEEDOR -->
                            @if(Session::has('report'))
                            <div class="col-md-8">
                                <div class="col-md-8">                                        
                                    <div class="caja">{{ Session::get('report') }}</div>
                                </div>
                            </div>
                            @endif
                            <!-- FIN NOTIFICACION AL REGISTRAR UN PROVEEDOR -->
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="form-horizontal">
                            <div class="col-md-12">

                                <!-- INICIO FORMULARIO-->
                                <form id="proveedorForm" method="POST" class="form-horizontal" action="{{ route('proveedor.update', ['$id' => $proveedor->prov_id]) }}">
                                    {{ csrf_field() }}
                                    @method('PUT')

                                    <!-- INICIO NOMBRE -->
                                    <div class="form-group {{ $errors->has('nombre') ? ' has-error' : '' }}" id="nombreProveedor">    
                                        <label class="col-md-3 control-label">Nombre</label>
                                        <div class="col-md-8">        
                                            <input type="text" name="nombre" id="prov_nombre"
                                            class="form-control" placeholder="Nombre del Proveedor" value="{{ $proveedor->prov_nombre }}" autofocus>
                                            @if ($errors->has('nombre'))
                                                <small class="form-text text-danger">{{ $errors->first('nombre') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- FIN NOMBRE -->

                                    <!-- INICIO IDENTIFICACION -->
                                    <div class="form-group">
                                        <label class="col-md-3 form-control-label">Tipo Documento</label>
                                        <div class="col-md-2">
                                            <select name="prov_tipoDoc" class="form-control">
                                                <option selected="true" value="{{ $proveedor->prov_tipoDoc }}" style="background-color: #A9A9A9">
                                                    {{ $proveedor->prov_tipoDoc }}
                                                </option>
                                                @foreach ($identificacion as $iden)
                                                    <option value="{{$iden->iden_codigo}}">
                                                        {{$iden->iden_codigo}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- FIN IDENTIFICACION -->

                                    <!-- INICIO NUMERO DE IDENTIFICACION -->
                                    <div class="form-group {{ $errors->has('prov_numDoc') ? ' has-error' : '' }}" id="nombreProveedor">    
                                        <label class="col-md-3 control-label">Número de Documento</label>
                                        <div class="col-md-4">        
                                            <input type="text" name="prov_numDoc" id="prov_numDoc"
                                            class="form-control" placeholder="Número de Documento del Proveedor" value="{{ $proveedor->prov_numDoc }}" autofocus>
                                            @if ($errors->has('prov_numDoc'))
                                                <small class="form-text text-danger">{{ $errors->first('prov_numDoc') }}</small>
                                            @endif
                                        </div>                                        
                                    </div>
                                    <!-- FIN NUMERO DE IDENTIFICACION -->

                                    <!-- INICIO DIRECCION-->
                                    
                                        @foreach ($direcciones as $indice => $direccion)
                                        @if($direccion != null)
                                            @if($indice == 0)
                                                <div class="form-group {{ $errors->has('direcciones.'.$indice)? ' has-error' : '' }}">
                                                    <label class="col-lg-3 control-label">Dirección</label>
                                                    <div class="col-md-2">
                                                        <select name="ciudades[]" class="form-control">
                                                            <option selected="true" style="background-color: #A9A9A9" value="{{ $direccion->dir_ciudad}}">
                                                                {{$direccion->dir_ciudad}}
                                                            </option>
                                                            @foreach ($ciudad as $ciu)
                                                                <option value="{{$ciu->ciu_descripcion}}">
                                                                    {{$ciu->ciu_descripcion}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input class="form-control" type="text" name="direcciones[]"  value="{{ $direccion->dir_descripcion }}" autofocus/>
                                                        @if ($errors->has('direcciones.'.$indice))
                                                            <small class="form-text text-danger">{{ $errors->first('direcciones.'.$indice) }}</small>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="button" class="btn btn-success addButton" data-template="direcciones">&nbsp; Mas +</button>
                                                    </div>
                                                </div>                                        
                                            @else                                        
                                                <div class="form-group {{ $errors->has('direcciones.'.$indice)? ' has-error' : '' }}">
                                                    <label class="col-lg-3 control-label">Dirección</label>
                                                    <div class="col-md-2">
                                                        <select name="ciudades[]" class="form-control">
                                                            <option selected="true" style="background-color: #A9A9A9" value="{{ $direccion->dir_ciudad }}">
                                                                {{ $direccion->dir_ciudad }}
                                                            </option>
                                                            @foreach ($ciudad as $ciu)
                                                                <option value="{{$ciu->ciu_descripcion}}">
                                                                    {{$ciu->ciu_descripcion}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input class="form-control" type="text" name="direcciones[]"  value="{{ $direccion->dir_descripcion }}" autofocus/>
                                                        @if ($errors->has('direcciones.'.$indice))
                                                            <small class="form-text text-danger">{{ $errors->first('direcciones.'.$indice) }}</small>
                                                        @endif
                                                    </div>                                                
                                                </div>
                                            @endif
                                        @else
                                        nada
                                        @endif
                                        @endforeach

                                    <!-- INICIO CLON -->
                                    <div class="form-group hide" id="direccionesTemplate">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-md-2">
                                            <select name="ciudades[]" class="form-control">
                                                @foreach ($ciudad as $ciu)
                                                    <option value="{{$ciu->ciu_descripcion}}">
                                                        {{$ciu->ciu_descripcion}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input class="form-control" type="text"/>
                                        </div>
                                        <div class="col-lg-1">
                                            <button type="button" class="btn btn-warning removeButton" id="btn_quitarDireccion" data-template="direcciones">Quitar -</button>
                                        </div>
                                    </div>
                                    <!-- FIN CLON -->
                                    <!-- FIN DIRECCION-->

                                    

                                    <!-- INICIO BOTONES GUARDAR O VOLVER -->
                                    <div class="modal-footer">
                                        <a href="{{ route('proveedor.edit' ,['$id' => $proveedor->prov_id]) }}">
                                            <button type="button" class="btn btn-warning">
                                                <i class="fa fa-exclamation fa-2x"></i>&nbsp;&nbsp;Limpiar
                                            </button>
                                        </a>
                                        <button id="btn_guardar" type="submit" class="btn btn-success"><i class="fa fa-save fa-2x"></i>&nbsp;Guardar</button> 
                                    </div>
                                    <!-- FIN BOTONES GUARDAR O VOLVER -->
                                </form>                                
                                <!-- FIN FORMULARIO-->
                            </div>                           

                        </div>
                    <!--Fin Contenido-->
                    </div>
                </div>                          
                            </div></<!--/container-fluid-->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@endsection